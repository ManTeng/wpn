﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace Bee.Core
{
    public sealed class Route
    {
        private PriorityRouteList entries = null;

        public Route()
        {
            entries = new PriorityRouteList();
            EventFilter.Add("Request_HasHandler", new Func<bool, HttpRequest, bool>(this.HasHandler));
            EventAction.Add("Request_HandleRequest", new Action<HttpContext>(this.HandleRequest));
        }

        public void Register(RouteData routeEntry)
        {
            entries[routeEntry.Pattern] = routeEntry;
        }

        public void Degister(RouteData entry)
        {
            if (entries.Contains(entry))
            {
                entries.Remove(entry);
            }
        }


        bool HasHandler(bool computedResult, HttpRequest request)
        {
            var count = entries.Count;
            for (int i = 0; i < count; i++)
            {
                var entry = entries[i];
                if (Regex.IsMatch(request.Url.PathAndQuery,entry.Pattern))
                {
                    return true;
                }
            }
            return false;
        }

        void HandleRequest(HttpContext context)
        {
            var count = entries.Count;
            var request = context.Request.Url.PathAndQuery;
            RouteData target = null;
            List<int> candidates = new List<int>();
            for (int i = 0; i < count; i++)
            {
                var entry = entries[i];
                if (Regex.IsMatch(request, entry.Pattern))
                {
                    if (request==entry.Pattern)
                    {
                        target = entry;
                        break;
                    }
                    if (context.Request.Path==entry.Dir)
                    {
                        candidates.Add(i);
                        break;
                    }
                    candidates.Add(i);
                }
            }
            if (target == null)
            {
                if (candidates.Count == 0)
                {
                    if (File.Exists(context.Server.MapPath("~/" + context.Request.Url.PathAndQuery)))
                    {
                        return;
                    }
                    else
                    {
                        context.Response.StatusCode = 404;
                        context.Response.End();
                    }
                }
                if (candidates.Count == 1)
                {
                    target = entries[candidates[0]];
                }
                else
                {
                    decimal score = 0, score2 = 0;
                    int p = 0;
                    for (int i = 0; i < candidates.Count; i++)
                    {
                        score = entries[candidates[i]].Caculate(request);
                        if (score2 < score)
                        {
                            score2 = score;
                            p = i;
                        }
                    }
                    target = entries[candidates[p]];
                }
            }
            

            context.Items["Router"] = target;

            try
            {
                if (target.ApplyAuthEvent)
                {
                    bool authenticated = false;
                    authenticated = EventFilter.Apply<bool>("User_Authentication", false, context.Request.Cookies);

                    if (!authenticated)
                    {
                        EventAction.Apply("User_Authentication_Failed");
                        context.Response.End();
                    }
                }
                target.Execution.Invoke(context);
                context.Response.End();
            }
            catch (Exception ex)
            {
                throw;
            }
            //context.Response.End();
        }
    }

    public class PriorityRouteList : IList<RouteData>
    {
        private List<RouteData> _container = null;

        public PriorityRouteList()
        {
            _container = new List<RouteData>();
        }

        public RouteData this[int index]
        {
            get
            {
                return _container[index];
            }

            set
            {
                _container[index] = value;
            }
        }

        public RouteData this[string pattern]
        {
            get
            {
                for (int i = 0; i < _container.Count; i++)
                {
                    if (_container[i].Pattern == pattern)
                    {
                        return _container[i];
                    }
                }
                return null;
            }
            set
            {
                int n = -1;
                for (int i = 0; i < _container.Count; i++)
                {
                    if (_container[i].Pattern == pattern)
                    {
                        n = i;
                    }
                }
                if (n >= 0)
                {
                    _container[n] = value;
                }
                else
                {
                    this.Add(value);
                }
            }
        }

        public int Count
        {
            get
            {
                return _container.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public void Add(RouteData item)
        {
            var priority = item.Priority;
            var count = _container.Count;
            int targetPosition = 0;
            for (int i = 0; i < count; i++)
            {
                if (_container[i].Priority <= priority)
                {
                    ++targetPosition;
                    continue;
                }
                else
                {
                    _container.Insert(i, item);
                    return;
                }
            }
            _container.Add(item);
        }

        public void Clear()
        {
            _container.Clear();
        }

        public bool Contains(RouteData item)
        {
            return _container.Contains(item);
        }

        public void CopyTo(RouteData[] array, int arrayIndex)
        {
            _container.CopyTo(array, arrayIndex);
        }

        public IEnumerator<RouteData> GetEnumerator()
        {
            return _container.GetEnumerator();
        }

        public int IndexOf(RouteData item)
        {
            return _container.IndexOf(item);
        }

        public void Insert(int index, RouteData item)
        {
             _container.Insert(index, item);
        }

        public bool Remove(RouteData item)
        {
            return _container.Remove(item);
        }

        public void RemoveAt(int index)
        {
             _container.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _container.GetEnumerator();
        }
    }

}
