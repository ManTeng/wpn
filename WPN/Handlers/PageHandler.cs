﻿using Bee.Core;
using System.IO;
using System.Web;
using WPN.Renders;

namespace WPN.Handlers
{
    public class PageHandler : IBeeHttpHandler
    {
        string[] defaultPages = { "default", "index" };
        char[] identifiers = { '?', '!', '#' };
        string[] exts = {"cshtml","vbhtml","ashx","aspx" };

        IRender render = null;

        public PageHandler(WPNApp wpn)
        {
            WPN = wpn;
            render = new PageRender(WPN);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public IRender Render
        {
            get
            {
                return render;
            }
        }

        public WPNApp WPN
        {
            get;set;
        }

        public void ProcessRequest(HttpContext context)
        {
            var routeEntry = context.Items["Router"] as RouteData;
            string pageToRend = "";

            
            
            if (routeEntry.Predicator!=null)
            {
                pageToRend = routeEntry.Predicator.Invoke();
            }
            if(pageToRend==null || pageToRend.Trim()==string.Empty)
            {
                pageToRend = FindPage(context.Request.Path);
            }
            if (string.IsNullOrEmpty(pageToRend) || !File.Exists(Utl.GetAbsPath(pageToRend)))
            {
                if (EventAction.Has("WPN_Http_Status_Handler"))
                {
                    EventAction.Apply("WPN_Http_Status_Handler", 404);
                }
                else
                {
                    context.Response.StatusCode = 404;
                    context.Response.End();
                }
                
            }
            render.RenderFile(pageToRend);
            
        }

        private string FindPage(string request)
        {
            var targetName= FindPageImpl(request, identifiers);
            if (targetName==request)
            {
                if (Directory.Exists(Utl.GetAbsPath("~" + request)))
                {
                    return defaultPage(HttpContext.Current.Request.Path);
                }
            }
            if (targetName.Contains("."))
            {
                return "~" + targetName;
            }
            return TryGetPhysicalFile(targetName);
        }

        string FindPageImpl(string request, char[] id, int n = 0)
        {
            if (n >= id.Length)
            {
                return request;
            }
            int pos = request.IndexOf(id[n]);
            if (pos == 0)
            {
                return defaultPage(HttpContext.Current.Request.Path);
            }
            if (pos > 0)
            {
                return request.Substring(0, pos);
            }
            ++n;
            return FindPageImpl(request, id, n);
        }

        string TryGetPhysicalFile(string targetName)
        {
            foreach (var ext in exts)
            {
                if (File.Exists(Utl.GetAbsPath("~" + targetName + "." + ext)))
                {
                    return "~" + targetName + "." + ext;
                }
            }
            return null;
        }

        string defaultPage(string path)
        {
            foreach (var ext in exts)
            {
                foreach (var defaultpage in defaultPages)
                {
                    if (File.Exists(Utl.GetAbsPath("~/" + path + defaultpage + "." + ext)))
                    {
                        return "~/" + path + defaultpage + "." + ext;
                    }
                }
            }
            return null;
        }
    }
}
