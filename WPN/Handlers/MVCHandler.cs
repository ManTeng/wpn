﻿using System;
using System.Web;

namespace WPN.Handlers
{
    public class MVCHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            //Get the RouteEntry
            //Find the controller according to the route item
            //Find the action
            // - If action is not specified, find the default action
            //   - If default action is not defined, use Index
            //Check if query string are there
            // - IF yes, check the action's parameter
            //    - Check the types and see if the first parameter is BaseTypes(string/int/....)
            //       - if yes, just parse the string to the type for the parameter
            //       - If not, create an instance for that type
            //          - Set the property of the type with the querystring
            //Invoke the action with/without the Model
            throw new NotImplementedException();
        }
    }
}
