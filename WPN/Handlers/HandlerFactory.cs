﻿using Bee.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using WPN.Handlers;

namespace WPN
{
    public  class HandlerFactory 
    {
        private Dictionary<string, Type> handlers = new Dictionary<string, Type>();
        private Dictionary<Type, IBeeHttpHandler> handlerInstances = new Dictionary<Type, IBeeHttpHandler>();

        public HandlerFactory(WPNApp app)
        {
            App = app;
        }

        public void RegisterHandler(string name, Type handlerType)
        {
            handlers[name] = handlerType;
        }

        public void UnRegisterHandler(string name)
        {
            handlers.Remove(name);
        }

        public IBeeHttpHandler GetHandlerType(string name)
        {
            if (handlers.ContainsKey(name))
            {
                var handlerType = handlers[name];
                if (!handlerInstances.ContainsKey(handlerType))
                {
                    handlerInstances[handlerType] = Activator.CreateInstance(handlerType,new object[] { App }) as IBeeHttpHandler;
                }
                return handlerInstances[handlerType];
            }
            return null;
        }

        public string[] GetRegistedHandlers()
        {
            return handlers.Keys.ToArray();
        }

        public Dictionary<string, Type> AllHandlers
        {
            get
            {
                return new Dictionary<string, Type>(handlers);
            }
        }

        public WPNApp App
        {
            get;set;
        }
    }

    public static class HandlerHelper
    {
        public static RouteData PageHandler(this RouteData routeEntry, Func<string> predicator = null)
        {
            IBeeHttpHandler handler = null;

            handler = routeEntry.App.HanderFactory.GetHandlerType("WebPage");
            if (handler == null)
            {
                throw new Exception("The WebPage handler is not found!");
            }
            routeEntry.Handler = handler;
            if (predicator != null)
            {
                routeEntry.Predicator = predicator;
            }
            routeEntry.Execution = (x) => { routeEntry.Handler.ProcessRequest(x); };
            return routeEntry;
        }
    }
}
