﻿using System.Web;

namespace WPN.Handlers
{
    public interface IBeeHttpHandler : IHttpHandler
    {
        IRender Render { get; }
    }
}
