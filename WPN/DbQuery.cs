﻿using System.Collections.Generic;
using WPN.DAL;

namespace WPN
{
    public static class DbQuery
    {
        private static IDbQuery inst = ResolveService.Create<IDbQuery>();

        public static IEnumerable<T> Query<T>(string sql, params object[] parameters)
        {
            return inst.Query<T>(sql, parameters);
        }

        public static void Execute(string sql, params object[] parameters)
        {
            inst.Execute(sql, parameters);
        }

    }
}
