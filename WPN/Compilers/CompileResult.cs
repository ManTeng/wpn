﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Bee.Core.Compilers
{
    public class CompileResult
    {
        public List<string> Output { get; set; }
        public bool Success { get; set; }
        public MemoryStream BinaryStream { get; set; }
        public string BinaryPath { get; set; }
    }
}
