﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System;
using System.Collections.Generic;
using System.IO;

namespace Bee.Core.Compilers
{
    public class RoslynCompiler : ICompiler
    {
        private static List<MetadataReference> init_references = null;
        public List<MetadataReference> SelfReference { set; get; }
        string BuildDIR = AppDomain.CurrentDomain.BaseDirectory + "BeeBuilder";

        static RoslynCompiler()
        {
            init_references = new List<MetadataReference>();
            var asms = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var item in asms)
            {
                init_references.Add(MetadataReference.CreateFromFile(item.Location));
            }
        }

        public RoslynCompiler()
        {
            SelfReference = new List<MetadataReference>(init_references);
        }

        public CompileResult Compile(string[] source, string asmName = null)
        {
            try
            {
                var AssemblyName = string.IsNullOrEmpty(asmName) ? "ASP_BeeBuilder_" + Path.GetRandomFileName() : asmName;

                var library_path = BuildDIR + Path.DirectorySeparatorChar + AssemblyName + ".dll";
                SyntaxTree[] syntaxTree = new SyntaxTree[source.Length];
                var i = 0;
                foreach (var item in source)
                {
                    syntaxTree[i] = CSharpSyntaxTree.ParseText(item);
                    ++i;
                }
                var cr = new CompileResult { Output = new List<string>() };
                CSharpCompilation compliation = CSharpCompilation.Create(asmName, syntaxTree, SelfReference, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
                var fs = new FileStream(library_path, FileMode.CreateNew, FileAccess.Write, FileShare.None);
                EmitResult result = compliation.Emit(fs);
                cr.Success = result.Success;
                if (!result.Success)
                {
                    foreach (Diagnostic item in result.Diagnostics)
                    {
                        cr.Output.Add(item.GetMessage());
                    }
                    EventAction.Apply("WPN_ERROR_SOURCE_ERRLIST", "RoslynCompiler.CompileCS", cr.Output);
                }
                else
                {
                    cr.BinaryPath = library_path;
                }
                return cr;
            }
            catch (Exception e)
            {
                EventAction.Apply("BEE_EXCEPTION_HANDLER", e);
                return null;
            }
            
        }

        public CompileResult CompileRazorPage(IEnumerable<string> files, IEnumerable<MetadataReference> additionalRef, string assemblyName = null)
        {
            WebPageRazorHost host = WebRazorHostFactory.CreateHostFromConfig(files.First());

            var pathKey = string.Join(";", files).GetHashCode().ToString();
            string library_path = null;
            var source_path = files;

            var filesNum = files.Count();
            var fileInfoList = new List<FileInfo>();
            int i = 0;

            foreach (var item in files)
            {
                var file = Utl.GetAbsPath(item);

                if (File.Exists(file))
                {
                    fileInfoList.Add(new FileInfo(Utl.GetAbsPath(item)));
                }
            }



            if (!BuilderLog.ShouldCompile(fileInfoList.ToArray(), pathKey, out library_path))
            {
                if (!_loadocks.ContainsKey(library_path))
                {
                    _loadocks[library_path] = new object();
                }
                return new CompileResult { BinaryPath = library_path, Success = true };
            }
            SyntaxTree[] syntaxTree = new SyntaxTree[fileInfoList.Count];
            i = 0;
            foreach (var item in fileInfoList)
            {
                syntaxTree[i] = CSharpSyntaxTree.ParseText(RazorCodeGenerator(host, item.FullName));
                ++i;
            }
            var compiler = new RoslynCompiler_old(pathKey);
            compiler.AssemblyName = assemblyName;
            if (additionalRef != null)
            {
                compiler.SelfReference.AddRange(additionalRef);
            }


            if (compiler.AssemblyName == null)
            {
                compiler.AssemblyName = "ASP_BeeBuilder_" + Path.GetRandomFileName();
            }
            var compile_lock = _buildLocks[pathKey];
            var cr = new CompileResult { Output = new List<string>() };

            lock (compile_lock)
            {
                if (library_path == null)
                {
                    library_path = compiler.BuildDIR + Path.DirectorySeparatorChar + compiler.AssemblyName + ".dll";
                }
                if (File.Exists(library_path))
                {
                    File.Delete(library_path);
                }
                if (!_loadocks.ContainsKey(library_path))
                {
                    _loadocks[library_path] = new object();
                }

                CSharpCompilation compliation = CSharpCompilation.Create(compiler.AssemblyName, syntaxTree, compiler.SelfReference, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
                var fs = new FileStream(library_path, FileMode.CreateNew, FileAccess.Write, FileShare.None);
                EmitResult result = compliation.Emit(fs);
                cr.Success = result.Success;
                if (!result.Success)
                {
                    if (result.Diagnostics.Length > 0 && result.Diagnostics[0].GetMessage().Contains("CS0101"))
                    {
                        cr.Success = true;
                    }
                    foreach (Diagnostic item in result.Diagnostics)
                    {
                        cr.Output.Add(item.GetMessage());
                    }
                }
                else
                {
                    foreach (var fi in fileInfoList)
                    {
                        BuilderLog.Write(pathKey, fi.LastWriteTimeUtc, library_path, pathKey);
                    }
                    cr.BinaryPath = library_path;
                }
                fs.Dispose();
            }
            return cr;
        }
    }
}
