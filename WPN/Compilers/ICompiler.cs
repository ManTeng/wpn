﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bee.Core.Compilers
{
    public interface ICompiler
    {
        CompileResult Compile(string[] source, string asmName = null);
    }
}
