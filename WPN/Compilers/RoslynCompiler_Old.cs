﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Razor;
using System.Web.WebPages.Razor;

namespace Bee.Core.Compilers
{
    public class RoslynCompiler_old
    {

        static readonly object _lock = new object();
        internal static string SystemDLLPath = Path.GetDirectoryName(typeof(object).Assembly.Location);
        static readonly Dictionary<string, object> _buildLocks = new Dictionary<string, object>();
        static readonly Dictionary<string, object> _loadocks = new Dictionary<string, object>();
        private static List<MetadataReference> init_references = null;

        public List<MetadataReference> SelfReference { set; get; }

        public Assembly Asm = null;
        string BuildDIR = AppDomain.CurrentDomain.BaseDirectory + "BeeBuilder";
        string pathKey = null;

        public string AssemblyName { get; set; }

        static RoslynCompiler_old()
        {
            init_references = new List<MetadataReference>();
            var asms = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var item in asms)
            {
                init_references.Add(MetadataReference.CreateFromFile(item.Location));
            }
        }

        RoslynCompiler_old(string _pathkey)
        {
            pathKey = _pathkey;

            lock (_lock)
            {
                if (!_buildLocks.ContainsKey(pathKey))
                {
                    _buildLocks[pathKey] = new object();
                }
                if (!Directory.Exists(BuildDIR))
                {
                    Directory.CreateDirectory(BuildDIR);
                }
            }
            SelfReference = new List<MetadataReference>(init_references);
        }

        public static CompileResult CompileCS(IEnumerable<string> files, IEnumerable<MetadataReference> additionalRef, string assemblyName = null)
        {
            var pathKey = string.Join(";", files).GetHashCode().ToString();
            string library_path = null;
            var source_path = files;

            var filesNum = files.Count();
            var fileInfoList = new FileInfo[filesNum];
            int i = 0;

            foreach (var item in files)
            {
                fileInfoList[i] = new FileInfo(item);
                ++i;
            }

            if (!BuilderLog.ShouldCompile(fileInfoList, pathKey, out library_path))
            {
                if (!_loadocks.ContainsKey(library_path))
                {
                    _loadocks[library_path] = new object();
                }
                return new CompileResult { BinaryPath = library_path, Success = true };
            }
            var compiler = new RoslynCompiler(pathKey);
            compiler.AssemblyName = assemblyName;
            if (additionalRef != null)
            {
                compiler.SelfReference.AddRange(additionalRef);
            }


            if (compiler.AssemblyName == null)
            {
                compiler.AssemblyName = "ASP_BeeBuilder_" + Path.GetRandomFileName();
            }
            var compile_lock = _buildLocks[pathKey];
            var cr = new CompileResult { Output = new List<string>() };

            lock (compile_lock)
            {
                if (library_path == null)
                {
                    library_path = compiler.BuildDIR + Path.DirectorySeparatorChar + compiler.AssemblyName + ".dll";
                }
                if (File.Exists(library_path))
                {
                    File.Delete(library_path);
                }
                if (!_loadocks.ContainsKey(library_path))
                {
                    _loadocks[library_path] = new object();
                }

                SyntaxTree[] syntaxTree = new SyntaxTree[filesNum];
                i = 0;
                foreach (var item in source_path)
                {
                    syntaxTree[i] = CSharpSyntaxTree.ParseText(File.ReadAllText(item));
                    ++i;
                }
                CSharpCompilation compliation = CSharpCompilation.Create(compiler.AssemblyName, syntaxTree, compiler.SelfReference, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
                var fs = new FileStream(library_path, FileMode.CreateNew, FileAccess.Write, FileShare.None);
                EmitResult result = compliation.Emit(fs);
                cr.Success = result.Success;
                if (!result.Success)
                {
                    foreach (Diagnostic item in result.Diagnostics)
                    {
                        cr.Output.Add(item.GetMessage());
                    }
                    EventAction.Apply("WPN_ERROR_SOURCE_ERRLIST", "RoslynCompiler.CompileCS", cr.Output);
                }
                else
                {
                    foreach (var item in source_path)
                    {
                        FileInfo fi = new FileInfo(item);
                        BuilderLog.Write(pathKey, fi.LastWriteTimeUtc, library_path, pathKey);
                        fi = null;
                    }
                    cr.BinaryPath = library_path;
                }
                fs.Dispose();
            }
            return cr;
        }

        public static CompileResult CompileRazorPage(IEnumerable<string> files, IEnumerable<MetadataReference> additionalRef, string assemblyName = null)
        {
            WebPageRazorHost host = WebRazorHostFactory.CreateHostFromConfig(files.First());

            var pathKey = string.Join(";", files).GetHashCode().ToString();
            string library_path = null;
            var source_path = files;

            var filesNum = files.Count();
            var fileInfoList = new List<FileInfo>();
            int i = 0;

            foreach (var item in files)
            {
                var file = Utl.GetAbsPath(item);

                if (File.Exists(file))
                {
                    fileInfoList.Add(new FileInfo(Utl.GetAbsPath(item)));
                }
            }



            if (!BuilderLog.ShouldCompile(fileInfoList.ToArray(), pathKey, out library_path))
            {
                if (!_loadocks.ContainsKey(library_path))
                {
                    _loadocks[library_path] = new object();
                }
                return new CompileResult { BinaryPath = library_path, Success = true };
            }
            SyntaxTree[] syntaxTree = new SyntaxTree[fileInfoList.Count];
            i = 0;
            foreach (var item in fileInfoList)
            {
                syntaxTree[i] = CSharpSyntaxTree.ParseText(RazorCodeGenerator(host, item.FullName));
                ++i;
            }
            var compiler = new RoslynCompiler_old(pathKey);
            compiler.AssemblyName = assemblyName;
            if (additionalRef != null)
            {
                compiler.SelfReference.AddRange(additionalRef);
            }


            if (compiler.AssemblyName == null)
            {
                compiler.AssemblyName = "ASP_BeeBuilder_" + Path.GetRandomFileName();
            }
            var compile_lock = _buildLocks[pathKey];
            var cr = new CompileResult { Output = new List<string>() };

            lock (compile_lock)
            {
                if (library_path == null)
                {
                    library_path = compiler.BuildDIR + Path.DirectorySeparatorChar + compiler.AssemblyName + ".dll";
                }
                if (File.Exists(library_path))
                {
                    File.Delete(library_path);
                }
                if (!_loadocks.ContainsKey(library_path))
                {
                    _loadocks[library_path] = new object();
                }

                CSharpCompilation compliation = CSharpCompilation.Create(compiler.AssemblyName, syntaxTree, compiler.SelfReference, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
                var fs = new FileStream(library_path, FileMode.CreateNew, FileAccess.Write, FileShare.None);
                EmitResult result = compliation.Emit(fs);
                cr.Success = result.Success;
                if (!result.Success)
                {
                    if (result.Diagnostics.Length > 0 && result.Diagnostics[0].GetMessage().Contains("CS0101"))
                    {
                        cr.Success = true;
                    }
                    foreach (Diagnostic item in result.Diagnostics)
                    {
                        cr.Output.Add(item.GetMessage());
                    }
                }
                else
                {
                    foreach (var fi in fileInfoList)
                    {
                        BuilderLog.Write(pathKey, fi.LastWriteTimeUtc, library_path, pathKey);
                    }
                    cr.BinaryPath = library_path;
                }
                fs.Dispose();
            }
            return cr;
        }

        internal static string RazorCodeGenerator(WebPageRazorHost host, string absFilePath)
        {
            var engine = new RazorTemplateEngine(host);
            GeneratorResults results = null;
            using (var stream = File.Open(absFilePath, FileMode.Open))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    results = engine.GenerateCode(reader, className: null, rootNamespace: null, sourceFileName: host.PhysicalPath);
                }
            }

            if (!results.Success)
            {
                return null;
            }

            // Use CodeDom to generate source code from the CodeCompileUnit
            var codeDomProvider = new CSharpCodeProvider();
            var srcFileWriter = new StringWriter();
            codeDomProvider.GenerateCodeFromCompileUnit(results.GeneratedCode, srcFileWriter, new CodeGeneratorOptions());

            return srcFileWriter.ToString();
        }



    }
}


