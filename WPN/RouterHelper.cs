﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using WPN;

namespace Bee.Core
{
    public static class RouterHelper
    {

        /*
        Rules for the Url Pattern Translation

            {ID} / {ID:CONSTRAINTS}

            CONSTRAINTS
                -- int/long/characters[0-9a-zA-Z]/string(default)/Guid
            
            Skipped chars(used in Javascript)
            #
            ?
            !
            %
        */
        public static RouteData Route(this WPNApp app, string urlPattern, int priority = 999)
        {
            if (string.IsNullOrEmpty(urlPattern))
            {
                throw new ArgumentNullException("urlPattern");
            }
            var pattern = urlPattern.Trim();
            var targetPattern = TranslateUrl(pattern);
            var entry = new RouteData(targetPattern) { Priority = priority, App = app };
            app.Routes.Register(entry);
            return entry;
        }

        public static RouteData Authorized(this RouteData entry)
        {
            entry.ApplyAuthEvent = true;
            return entry;
        }

        /// <summary>
        /// Set the status and stop Rendering();
        /// </summary>
        /// <param name="context"></param>
        /// <param name="status"></param>
        public static void Status(this HttpContext context, int status)
        {
            EventAction.Apply("Request_Status_Process", context, status);
            context.Response.StatusCode = status;
            context.Response.End();
        }

        public static string TranslateUrl(string urlPattern)
        {
            var replaced = "";
            replaced = Regex.Replace(urlPattern, @"\{\*.*\}", ".*");
            //replaced = replaced.Replace("/.*", "(/.*)?");
            return replaced;
        }
        
        public static RouteData Redirect(this RouteData route, string url)
        {
            route.Execution = (x) => { x.Response.Redirect(url); };
            return route;
        }

        public static void Remove(this RouteData entry)
        {
            if (entry != null && entry.App!=null)
            {
                entry.App.Routes.Degister(entry);
            }
        }

    }
}
