﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Bee.Core
{
    /// <summary>
    /// Translation implementation for the application. This is not thread safe, the class is not designed for mutiple thread environment.
    /// </summary>
    public class Language
    {

        Dictionary<string, Language> dictionaries = new Dictionary<string, Language>();
        string domain = null;
        Dictionary<string, Translator> translators = new Dictionary<string, Translator>();

        public Language(string domain)
        {
            this.domain = domain;
            dictionaries[this.domain] = this;    
        }
                

        public Translator GetTranslator(string language)
        {
            if (translators.ContainsKey(language))
            {
                return translators[language];
            }
            return null;
        }

        public bool AddTranslation(string language, string poFile)
        {
            var translator = new Translator(language);
            if (translator.LoadTranslation(poFile))
            {
                translators[language] = translator;
                return true;
            }
            return false;
        }
    }

    public class Translator {
        private Dictionary<string, string> dictionary = new Dictionary<string, string>();
        private string language = null;

        public Translator(string targetLanguage)
        {
            language = targetLanguage;
        }

        public string T(string source)
        {
            if (dictionary.ContainsKey(source))
            {
                return dictionary[source];
            }
            return source;
        }

        public bool LoadTranslation(string poFile)
        {
            if (File.Exists(poFile))
            {
                var fc = File.ReadAllText(poFile, System.Text.Encoding.UTF8);
                var fileConent = File.ReadAllLines(poFile, System.Text.Encoding.UTF8);
                string k = null, v = null;
                for (int i = 0; i < fileConent.Length; i++)
                {
                    var line = fileConent[i];
                    if (line.StartsWith("#") || line.StartsWith("\"") || line.Trim() == string.Empty)
                    {
                        continue;
                    }
                    if (line.StartsWith("msgid"))
                    {
                        try
                        {
                            var p = line.IndexOf(" \"");
                            if (p == line.Length - 1 || p != 5)
                            {
                                return false;
                            }

                            if (line[7] == '\"')
                            {
                                k = string.Empty;
                            }
                            else
                            {
                                k = line.Substring(7, line.Length - 8);
                            }
                        }
                        catch (Exception e)
                        {
                            return false;
                        }
                    }
                    if (line.StartsWith("msgstr"))
                    {
                        if (k == null)
                        {
                            return false;
                        }
                        try
                        {
                            var p = line.IndexOf(" \"");
                            if (p == line.Length - 1 || p != 6)
                            {
                                return false;
                            }

                            if (line[8] == '\"')
                            {
                                v = string.Empty;
                            }
                            else
                            {
                                v = line.Substring(8, line.Length - 9);
                            }
                            if (v != string.Empty)
                            {
                                dictionary[k] = v;
                            }
                            k = null;
                            v = null;
                        }
                        catch (Exception e)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        }
    }
    
}
