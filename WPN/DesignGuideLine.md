﻿# 1. Routing
## 1.1 Default routing handler

### 1.1.1 Events

+ Request_HasHandler

+ Request_HandleRequest

### 1.1.2 WebPage
WebPage description Here
### 1.1.3 MVC
MVC way description here

## 1.2 The ASP.Net Request Cycle

- Context_BeginRequest
- Context_ResovleRequestCache

# 2. Plugin
Plugin is the key feature of this framework. 

All your logics are developed as plugins and be executed by the plugin manager during runtime.

There are some pre-defined events that will be used in Framework level. Some of them are MUST be implemented by you, and others are depended on your needs.

For the MUST be implemented events, see Required Events.

## 2.1 Implementing a plugin
### Plugin Structure
### Plugin Info
### Activing Plugin
* Active(PluginInfo pi) 
* PluginInfo
    

## 2.2 Plugin Events
### Plugin_GetActived

Event type|Parameters|Return
----|----|----
Filter|N/A|string[]
This event is called when the plugin manager tries to load all the actived plugins.

Please provide the plugin manager with the actived plugins' names at the end of your code.


### Plugin_GetExecutor

__Get the plugin executor by the plugin name.__

Event type|Parameters|Return
----|----|----
Filter|2|string

+ This filter should return a string, which is the executor of plugin.
+ The executor could either a webpage, a web form or a class.
+ The executor could be written in C# or VB.Net.

Parameter Order|Name|Type
------|----|----
1|Computed_Plugin_Executor|string
2|PluginName|string


### Plugin_Enabling
+ Persistant storage the information of the plugin going to be enabled

Event type|Return
----|----
Action|N/A
# 3. Theme
# 4. International