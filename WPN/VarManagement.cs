﻿using System.Collections.Generic;

namespace Bee.Core
{
    public class VarManagement
    {
        private Dictionary<string, string> settings = new Dictionary<string, string>();


        public void Set(string key, string v)
        {
            settings[key] = v;
            EventAction.Apply("VarManagement_Set", key, v);
        }

        public bool Has(string v)
        {
            return EventFilter.Apply<bool>("VarManagement_Has", false, v);
        }

        public string Get(string key)
        {
            if (settings.ContainsKey(key))
            {
                return settings[key];
            }
            else
            {
                var value = EventFilter.Apply<string>("VarManagement_Get", string.Empty, key);
                if (value != null && value.Trim() != string.Empty)
                {
                    settings[key] = value;
                    return value;
                }
            }
            return null;
        }

        public void SerializeData()
        {
            EventAction.Apply("VarManagement_SerializeData", settings);
        }

        public Dictionary<string,string> Settings { get { return new Dictionary<string, string>(settings); } }

        public void RestoreData()
        {
            EventAction.Apply("VarManagement_DeSerializeData",settings);
        }

        public string this[string index]
        {
            get {
                if (settings.ContainsKey(index))
                {
                    return settings[index];
                }
                return null;
            }
            set {
                if (value==null)
                {
                    settings.Remove(index);
                }
                else
                {
                    settings[index] = value;
                }
                
            }
        }

    }
}
