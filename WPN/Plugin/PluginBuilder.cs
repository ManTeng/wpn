﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Diagnostics;

namespace Bee.Core
{
    public class PluginBuilder
    {
        public string PluginPath { get; set; }
        public AppService App { get; set; }
        public ProjectJson ProjectDef { get; set; }

        Compilers.CompileResult BuildResult;
        public Assembly Asm = null;

        public PluginBuilder(string pluginPath, AppService app)
        {
            PluginPath = pluginPath;
            ProjectDef = GetPluginJson();
            App = app;
        }

        public ProjectJson GetPluginJson()
        {
            var prjFile = PluginPath + Utl.SEP + "plugin.json";
            if (!File.Exists(prjFile))
            {
                return null;
            }
            ProjectJson pj = null;
            try
            {
                var prjson = File.ReadAllText(prjFile);
                pj = SimpleJSON.Parser.Parse<ProjectJson>(prjson);
            }
            catch (Exception e)
            {
                EventAction.Apply("BEE_EXCEPTION_HANDLER", "Builder", e);
            }
            return pj;
        }

        public Compilers.CompileResult Build()
        {
            var extraRef = new List<MetadataReference>();
            if (ProjectDef != null && ProjectDef.References != null)
            {
                var references = ProjectDef.References;
                if (references.System != null && references.System.Count > 0)
                {
                    foreach (var item in references.System)
                    {
                        extraRef.Add(MetadataReference.CreateFromFile(Utl.SystemDLLPath + Utl.SEP + item));
                    }
                }
                if (references.Custom != null && references.Custom.Count > 0)
                {
                    foreach (var item in references.Custom)
                    {
                        extraRef.Add(MetadataReference.CreateFromFile(PluginPath + Utl.SEP + item));
                    }
                }
            }
            var including = new List<string>();

            if (ProjectDef.Including!=null)
            {
                BuildResult = EventFilter.Apply<Compilers.CompileResult>("Compile", null, ProjectDef.Clone(), extraRef, null);
                return BuildResult;
            }
            return null;
        }

        public object GetInstance(string typeName)
        {            
            return Activator.CreateInstance(GetType(typeName), App);
        }

        public Type GetType(string typeName)
        {
            if (Asm == null)
            {
                if (!string.IsNullOrEmpty(BuildResult.BinaryPath))
                {
                    Stopwatch watch = new Stopwatch();
                    watch.Start();
                    Asm = Assembly.LoadFile(BuildResult.BinaryPath);
                    watch.Stop();
                    Debug.WriteLine("LoadFile used " + watch.ElapsedMilliseconds / 1000 + " s");
                }
                else
                {
                    if (BuildResult.BinaryStream != null)
                    {
                        Assembly.Load(BuildResult.BinaryStream.ToArray());
                    }
                    else
                    {
                        throw new Exception("BinaryPath or BinaryStream should at least specify one");
                    }
                }
            }
            return Asm.GetType(typeName);
        }

        public dynamic Getfunctions()
        {
            return GetInstance(ProjectDef.PluginInfo.PluginName + "_functions");
        }

        public void LoadAssembly()
        {
            if (ProjectDef.PluginInfo.PluginAssembly!=null)
            {
                Asm = Assembly.LoadFile(PluginPath + Utl.SEP +  ProjectDef.PluginInfo.PluginAssembly);
            }
        }

        public void LoadReferences()
        {
            if (ProjectDef.References!=null && ProjectDef.References.Custom!=null && ProjectDef.References.Custom.Count > 0)
            {
                var reflist = AppDomain.CurrentDomain.GetAssemblies().Select(x => x.GetName().Name).Distinct().ToList();
                var loadedRef = new HashSet<string>();
                reflist.ForEach(x => loadedRef.Add(x));
                if (ProjectDef.References != null && ProjectDef.References.Custom != null && ProjectDef.References.Custom.Count > 0)
                {
                    foreach (var item in ProjectDef.References.Custom)
                    {
                        if (!loadedRef.Contains(item))
                        {
                            //var asm = Assembly.LoadFile(PluginPath + Utl.SEP + item);
                            if (!File.Exists(Utl.GetAbsPath("~/bin/") + item))
                            {
                                File.Copy(PluginPath + Utl.SEP + item, Utl.GetAbsPath("~/bin/") + item, false);
                            }
                        }
                    }
                }
            }
        }
    }
}
