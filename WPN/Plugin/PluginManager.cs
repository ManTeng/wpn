﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Bee.Core
{
    public class PluginManager
    {
        public AppService App { get; set; }

        public string PluginFolderPath { get; set; }
        public string AbsPluginFolderPath { get; set; }

        public Dictionary<string,PluginBuilder> ActivatedPlugins { get; set; }

        public PluginManager(AppService app)
        {
            App = app;
            ActivatedPlugins = new Dictionary<string, PluginBuilder>();
        }

        public string SantilizePluginName(string pluginName)
        {
            return pluginName.Replace(' ', '_');
        }

        public void LoadRequestPlugins()
        {
            var plugins = EventFilter.Apply<IEnumerable<string>>("Plugin_GetActived");
            if (plugins != null)
            {
                foreach (var pluginName in plugins)
                {
                    var pluginDir = SantilizePluginName(pluginName);
                    try
                    {
                        var pb = new PluginBuilder(Utl.GetAbsPath(PluginFolderPath + Utl.SEP + pluginDir),App);
                        dynamic func = pb.Getfunctions();
                        func.Active(App);
                    }
                    catch (Exception e)
                    {
                        EventAction.Apply("BEE_EXCEPTION_HANDLER", "LoadRequestPlugins", e);
                    }
                }
            }
        }

        public IList<ProjectJson> GetPluginLists()
        {
            var pluginDirs = Directory.GetDirectories(AbsPluginFolderPath);
            var result = new List<ProjectJson>();
            foreach (var dir in pluginDirs)
            {
                ProjectJson obj = null;
                obj = GetPluginInfo(dir);
                if (obj != null)
                {
                    result.Add(obj);
                }
            }
            return result;
        }

        ProjectJson GetPluginInfo(string pluginDirName)
        {
            var path = AbsPluginFolderPath + Utl.SEP + pluginDirName;
            if (!Directory.Exists(path))
            {
                return null;
            }
            var prjDef = path + Utl.SEP + "plugin.json";
            if (!File.Exists(prjDef))
            {
                return null;
            }
            return SimpleJSON.Parser.Parse<ProjectJson>(prjDef);
        }

        public bool IsActived(string pluginName)
        {
            return ActivatedPlugins.ContainsKey(pluginName);
        }

        public void Active(string pn)
        {

            if (ActivatedPlugins.ContainsKey(pn))
            {
                return;
            }

            var pb = new PluginBuilder(Path.Combine(AbsPluginFolderPath, pn),App);

            var pi = pb.ProjectDef.PluginInfo;
            pb.LoadReferences();

            if (string.IsNullOrEmpty(pi.PluginAssembly) && !string.IsNullOrEmpty(pi.PluginExecutor))
            {
                pb.Build();
            }
            else
            {
                pb.LoadAssembly();
            }
            if (!string.IsNullOrEmpty(pi.PluginExecutor))
            {
                var plugin = pb.GetInstance(pi.PluginExecutor) as IPlugin;
                plugin.Activate(App);
            }
            else
            {
                var funcs = pb.Getfunctions();
                funcs.Activate(App);
            }


            ActivatedPlugins[SantilizePluginName(pn)] = pb;
        }
    }
}
