﻿using System.CodeDom.Compiler;
using System.IO;
using System.Web.Razor;
using System.Web.WebPages.Razor;

namespace WPN
{
    public class RazorCodeGenerator 
    {
        public static string GenerateCode(WebPageRazorHost host, string absFilePath)
        {
            var engine = new RazorTemplateEngine(host);
            GeneratorResults results = null;
            using (var stream = File.Open(absFilePath, FileMode.Open))
            {
                using (TextReader reader = new StreamReader(stream))
                {
                    results = engine.GenerateCode(reader, className: null, rootNamespace: null, sourceFileName: host.PhysicalPath);
                }
            }

            if (!results.Success)
            {
                return null;
            }

            // Use CodeDom to generate source code from the CodeCompileUnit
            var codeDomProvider = new Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider();
            var srcFileWriter = new StringWriter();
            codeDomProvider.GenerateCodeFromCompileUnit(results.GeneratedCode, srcFileWriter, new CodeGeneratorOptions());

            return srcFileWriter.ToString();
        }
    }
}
