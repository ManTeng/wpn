﻿using Bee.Core;
using System;
using System.Web;

namespace WPN.Renders
{
    public class JsonPRender : IRender
    {
        public JsonPRender(WPNApp app)
        {
            App = app;
        }
        public WPNApp App
        {
            get;set;
        }

        public void RenderContent(object content, object callback = null)
        {
            var context = HttpContext.Current;
            context.Response.CacheControl = "no-cache";
            if (callback==null)
            {
                context.Response.ContentType = "text/javascript";
                context.Response.Write(EventFilter.Apply<string>("TOJSON", null, content));
            }
            else
            {
                context.Response.ContentType = "text/javascript";
                context.Response.Write(callback.ToString() + "(" + EventFilter.Apply<string>("TOJSON", null, content) + ")");
            }
            context.Response.End();
        }

        public void RenderFile(string fileName)
        {
            throw new NotImplementedException();
        }
    }
}
