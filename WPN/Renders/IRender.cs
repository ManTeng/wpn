﻿using System;

namespace WPN
{
    public interface IRender
    {
        void RenderFile(string fileName);
        void RenderContent(object content, object model = null);
    }
}
