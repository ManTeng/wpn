﻿using Bee.Core;
using System;
using System.Web;
using System.Web.WebPages;

namespace WPN.Renders
{
    public class PageRender : IRender
    {

        string defaultPage = "default";
        char[] identifiers = { '?', '!', '#' };
        string[] exts = { "cshtml", "vbhtml", "ashx", "aspx" };

        public WPNApp App
        {
            get;set;
        }
        public PageRender(WPNApp app)
        {
            App = app;
        }

        public void RenderContent(object content, object model = null)
        {
            throw new NotImplementedException();
        }

        public void RenderFile(string fileName)
        {
            var context = HttpContext.Current;
            var routeEntry = context.Items["Router"] as RouteData;
            WebPage page = BeeBuilder.BuildRazorPage(new string[] { fileName }, null, null);

            page.VirtualPath = fileName;
            WebPageRenderingBase startPage = StartPage.GetStartPage(page, "_PageStart", new string[] { "cshtml", "vbhtml" });
            WebPageContext pageContext = new WebPageContext(new HttpContextWrapper(context), null, null);
            page.ExecutePageHierarchy(pageContext, context.Response.Output, startPage);

            return;
        }
    }
}
