﻿using Bee.Core;
using System;
using System.Collections.Generic;
using System.IO;

namespace WPN
{
    public class ThemeManager
    {
        Dictionary<string, PluginBuilder> themeBuilders = new Dictionary<string, PluginBuilder>();
        public WPNApp WPN { get; set; }

        public ThemeManager(WPNApp wpn)
        {
            this.WPN = wpn;
        }

        public PluginBuilder GetThemeBuilder(string themeName)
        {
            if (themeBuilders.ContainsKey(themeName))
            {
                return themeBuilders[themeName];
            }
            return null;
        }

        public void Active(string themeName)
        {
            var dir = Utl.GetAbsPath(GetThemePath(themeName));
            var pbuilder = new PluginBuilder(dir,WPN.App);
            themeBuilders[themeName] = pbuilder;
            pbuilder.LoadReferences();
            pbuilder.Build();
            dynamic fun = pbuilder.GetInstance( WPN.App.Plugins.SantilizePluginName(themeName).Replace(".",string.Empty) + "_functions");
            fun.Activate(WPN);
        }

        public void Deinstall(string themeName)
        {

        }

        public void Install(string themeName)
        { }

        public string GetThemePath(string themeName)
        {
            return string.Format("~{0}WPN-Content{0}Themes{0}{1}{0}", "/", themeName);
        }

        public string ThemeDir
        {
            get
            {
                return string.Format("~{0}WPN-Content{0}Themes{0}{1}{0}", "/", WPN.DbCfg.SiteTheme);
            }
        }

        public IList<ThemeInfo> GetThemeList()
        {
            return GetThemeList(ThemeDir);
        }

        public IList<ThemeInfo> GetThemeList(string pluginRoot)
        {
            throw new NotImplementedException();
        }

        private ThemeInfo GetThemeInfo(string dir)
        {
            /*
            We will follow the wordpress
            ---------------------------------------------
            1. Style information stored in style.css under Theme directory
            2. A functional file(functions.cs)
            3. A screen shot image(.png)
            */
            var style_css = dir + Path.DirectorySeparatorChar + "style.css";
            if (File.Exists(style_css))
            {
                var lines = File.ReadAllLines(style_css);
                if (lines[0] == "/*" && lines[10] == "*/")
                {
                    var themeInfo = new ThemeInfo();
                    for (int i = 1; i < 10; i++)
                    {
                        var line = lines[i].Trim();

                        if (line.StartsWith("Theme Name"))
                            themeInfo.ThemeName = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("Theme URI"))
                            themeInfo.ThemeURI = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("Description"))
                            themeInfo.Desc = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("Author"))
                            themeInfo.Author = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("Author URI"))
                            themeInfo.AuthorURI = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("Version"))
                        {
                            var versionStr = line.Substring(line.IndexOf(':')).Trim();
                            var items = versionStr.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                            int p = 0;
                            Int32.TryParse(items[0], out p);
                            themeInfo.MajorVersion = p;
                            Int32.TryParse(items[0], out p);
                            themeInfo.MinorVersion = p;
                        }
                        if (line.StartsWith("License"))
                            themeInfo.License = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("License URI"))
                            themeInfo.LicenseURI = line.Substring(line.IndexOf(':')).Trim();
                        if (line.StartsWith("Tags"))
                            themeInfo.Tags = line.Substring(line.IndexOf(':')).Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                    return themeInfo;
                }
            }
            return null;
        }
    }
}
