﻿using System.IO;
using Bee.Core;
using Bee.Core.FileStorage;

namespace Bee.Core
{
    public abstract class FileStorageBbase
    {
       
        public abstract bool Exists(string fileName);
        public abstract string GetStoragePath(string fileExtension);
        public abstract PhsicalFile Save(string fileName, Stream stream);
        public abstract PhsicalFile Save(string fileName, byte[] file);
        public abstract void Delete(string fileName);

        public static FileStorageBbase Create(string provider)
        {
            //var context = new Models.FootBallLeagueContext();
            //var fileStorage = (from f in context.Config where f.Item == "FileStorage" select f.Value).First();
            //if (fileStorage == "LocalFileStorage") return new LocalFileStorage();
            //return new AzureFileStorage();
            return new LocalFileStorage();
        }
    }
}