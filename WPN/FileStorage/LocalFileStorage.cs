﻿using Bee.Core;
using System;
using System.IO;

namespace Bee.Core.FileStorage
{
    public class LocalFileStorage : FileStorageBbase
    {
        private string folder = "";

        public LocalFileStorage()
        {
            folder = EventFilter.Apply<string>("LocalStorageFolderPath");
        }

        public override PhsicalFile Save(string fileName, byte[] file)
        {
            if (File.Exists(folder + Path.DirectorySeparatorChar + fileName))
            {
                File.Delete(fileName);
            }
            using (var fs = File.Create(fileName))
            {
                fs.Write(file, 0, file.Length);
                fs.Flush();
            }
            return new PhsicalFile { FilePath = folder + Path.DirectorySeparatorChar + fileName, FileName = fileName, UploadDate = DateTime.UtcNow };
        }

        public override PhsicalFile Save(string fileName, Stream stream)
        {
            if (File.Exists(folder + Path.DirectorySeparatorChar + fileName))
            {
                File.Delete(fileName);
            }
            using (var file = File.Create(fileName))
            {
                stream.CopyTo(file);
                file.Flush();
            }
            return new PhsicalFile { FilePath = folder + Path.DirectorySeparatorChar + fileName, FileName = fileName, UploadDate = DateTime.UtcNow };
        }

        public override void Delete(string fileName)
        {
            if (File.Exists(folder + Path.DirectorySeparatorChar + fileName))
            {
                File.Delete(fileName);
            }
        }

        public override bool Exists(string fileName)
        {
            var ext = Path.GetExtension(fileName);
            var phsicalPath = GetStoragePath(ext) + Path.DirectorySeparatorChar + fileName;
            return File.Exists(phsicalPath);
        }

        public override string GetStoragePath(string ext)
        {
            return folder + "bin";
        }
    }
}