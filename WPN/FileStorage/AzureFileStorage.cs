﻿//using WPN.Models;
//using Microsoft.WindowsAzure.Storage;
//using Microsoft.WindowsAzure.Storage.Blob;
//using System;
//using System.Configuration;
//using System.IO;


//namespace WPN.FileStorage
//{
//    public class AzureFileStorage : FileStorage
//    {
//        private CloudStorageAccount account      = null;
//        private CloudBlobClient     blobClient   = null;
//        private CloudBlobContainer  imageContainer    = null;
//        private CloudBlobContainer binContainer = null;
//        private CloudBlobContainer htmlContainer = null;
//        private string azureStoragePath = null;

//        public AzureFileStorage()
//        {
//            account = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["azure_storage_connstr"]);
//            blobClient = account.CreateCloudBlobClient();
//            imageContainer = blobClient.GetContainerReference("images");
//            htmlContainer = blobClient.GetContainerReference("html");
//            binContainer = blobClient.GetContainerReference("bin");
//            imageContainer.CreateIfNotExists();
//            htmlContainer.CreateIfNotExists();
//            binContainer.CreateIfNotExists();
//            imageContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
//            htmlContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
//            binContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
//            //var context = new Models.FootBallLeagueContext();
//            azureStoragePath = SiteConfig.GetConfig("AzureStorageFolderPath");
//        }

//        private CloudBlockBlob GetClobReference(string fileName)
//        {
//            var ext = fileName.ExtractFileExtension();
//            if (SiteConfig.HtmlFolderExtensions.Contains(ext))
//            {
//                return htmlContainer.GetBlockBlobReference(fileName);
//            }
//            else if (SiteConfig.ImageFolderExtensions.Contains(ext))
//            {
//                return imageContainer.GetBlockBlobReference(fileName);
//            }
//            return imageContainer.GetBlockBlobReference(fileName);
//        }

//        public override PhsicalFile Save(string fileName, byte[] file)
//        {
//            CloudBlockBlob blob = GetClobReference(fileName);
//            blob.DeleteIfExists();
//            blob.UploadFromStream(new MemoryStream(file));
            
//            return new PhsicalFile { UploadDate = DateTime.UtcNow, FileName = fileName, FilePath = azureStoragePath + fileName  };
//        }

//        public override PhsicalFile Save(string fileName, Stream stream)
//        {
//            var blob = GetClobReference(fileName);
//            blob.DeleteIfExists();
//            blob.UploadFromStream(stream);
//            return new PhsicalFile { UploadDate = DateTime.UtcNow, FileName = fileName, FilePath = azureStoragePath + fileName };
//        }

//        public override void Delete(string fileName)
//        {
//            var blob = GetClobReference(fileName);
//            blob.DeleteIfExists();
//        }

//        public override bool Exists(string fileName)
//        {
//            return  GetClobReference(fileName).Exists();
//        }

//        public override string GetStoragePath(string ext)
//        {
//            if (SiteConfig.HtmlFolderExtensions.Contains(ext))
//            {
//                return azureStoragePath + "html";
//            }
//            else if (SiteConfig.ImageFolderExtensions.Contains(ext))
//            {
//                return azureStoragePath + "images";
//            }
//            return azureStoragePath + "bin";
//        }
//    }
//}