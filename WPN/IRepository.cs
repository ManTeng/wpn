﻿namespace WPN
{
    public interface IRepository<T> where T : class
    {
        bool Add(T item);
        bool Add(T[] items);
        bool Remove(T item);
        bool Update(T item);
        T GetByID(int id);
        bool CheckExistByID(int id);
    }
}
