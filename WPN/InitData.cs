﻿using WPN.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPN
{
    public class InitData
    {
        public static bool Inited = false;

        public static void Do(WPNApp wpn)
        {

            Term system = new Term { TermName = "Sys", UrlAbbr = null, Desc = "系统所需" };
            Term custom_page = new Term { TermName = "Custom", UrlAbbr = null, Desc = "自定义页面" };
            Term taxonomy = new Term { TermName = "Taxonomy", UrlAbbr = null, Desc = "Category" };
            Term trash = new Term { TermName = "Trash", UrlAbbr = null, Desc = "Deleted" };
            Term tag = new Term { TermName = "Tag", UrlAbbr = "Tag", Desc = "Tags" };
            Term menu = new Term { TermName = "Menu",UrlAbbr=null, Desc="Menus" };
            Term.Add(new[] { custom_page, system, trash, taxonomy, tag, menu });

            Taxonomy uncategoried = new Taxonomy { TaxonomyName = "Uncategoried", UrlAbbr = "uncategoried", TermID = taxonomy.TermID };
            
            Taxonomy systemPage = new Taxonomy { TermID = system.TermID, TaxonomyName = "sysPage", UrlAbbr = null };
            Taxonomy.Add(new[] { uncategoried, systemPage/*, league, soccer, team, country*/ });

            //Taxonomy englandTaxonomy = new Taxonomy { TermID = taxonomy.TermID, TaxonomyName = "England", UrlAbbr = "England", ParentID = country.TaxonomyID };
            //Taxonomy germanyTaxonomy = new Taxonomy { TermID = taxonomy.TermID, TaxonomyName = "Germany", UrlAbbr = "Germany", ParentID = country.TaxonomyID };
            //Taxonomy primerLeagueTaxonomy = new Taxonomy { TermID = taxonomy.TermID, TaxonomyName = "Primer", UrlAbbr = "PrimerLeague", ParentID = league.TaxonomyID };
            //Taxonomy bundesLeagueTaxonomy = new Taxonomy { TermID = taxonomy.TermID, TaxonomyName = "Bundes", UrlAbbr = "BundesLeague", ParentID = league.TaxonomyID };
            //Taxonomy italyLeagueTaxonomy = new Taxonomy { TermID = taxonomy.TermID, TaxonomyName = "Italy", UrlAbbr = "ItalyLeague", ParentID = league.TaxonomyID };
            //Taxonomy muTeamTaxonomy = new Taxonomy { TermID = taxonomy.TermID, TaxonomyName = "MU", ParentID = primerLeagueTaxonomy.TaxonomyID, UrlAbbr = "Man_United" };
            //Taxonomy.Add(new[] { englandTaxonomy, germanyTaxonomy, primerLeagueTaxonomy, bundesLeagueTaxonomy, muTeamTaxonomy, italyLeagueTaxonomy });

            //Page index = new Page { TermID = system.TermID, PageName = "index", PageTitle = "Hello", PageContent = "<div class=\"row\"><h1>默认首页</h1></div>", PageStatus = PageStatus.Posted };
            //Page undefined = new Page { TermID = system.TermID, PageName = "undefined", PageTitle = "找不到网页", PageContent = "<div class=\"row\"><h1>找不到指定的网页，看来它还没有被建立。</h1></div>", PageStatus = PageStatus.Posted };
            //Page arsenalPage = new Page { TermID = custom_page.TermID, PageName = "arsenal", PageTitle = "阿森纳俱乐部", PageContent = "====阿森纳", PageStatus = PageStatus.Posted };
            //Page muPage = new Page { TermID = custom_page.TermID, PageName = "Manchest United", PageTitle = "曼联俱乐部", PageContent = "====曼联", PageStatus = PageStatus.Posted };
            //Page bvbPage = new Page { TermID = custom_page.TermID, PageName = "BVB", PageTitle = "多特蒙德俱乐部", PageContent = "====多特蒙德", PageStatus = PageStatus.Posted };
            //Page soccer_rvp = new Page { TermID = custom_page.TermID, PageName = "范佩西", PageTitle = "范佩西", PageContent = "====范佩西", PageStatus = PageStatus.Posted };
            //Page trashPage = new Page { TermID = system.TermID, PageName = "TrashedPage", PageTitle = "被删除的页面", PageContent = "默认在被删除的列表", PageStatus = PageStatus.Trash };
            //Page bundes_league_page = new Page { TermID = custom_page.TermID, PageName = "BundesLeague", PageTitle = "范佩西", PageContent = "====范佩西", PageStatus = PageStatus.Posted };
            //Page categoryIndex = new Page { TermID = systemPage.TermID, PageName = "category_index", PageTitle = "st", PageContent = "using System.Web.Mvc;\r\n@foreach (var item in ViewBag.Leagues) {<li><a href=\"@Url.Content(\"~/\" + item.UrlAbbr)\" > @item.TaxonomyName </a></li>}", PageStatus = PageStatus.Posted };
            //Page.Add(new[] { index, undefined, arsenalPage, muPage, bvbPage, soccer_rvp, bundes_league_page, trashPage, categoryIndex });

            //SiteConfig siteName = new SiteConfig
            //{
            //    Item = "SiteName",
            //    Value = "足球信息",
            //    ItemLabel = "网站名称",
            //    SystemConfig = true,
            //    SettingGroup = "WPN",
            //    Description = "Site name"
            //};
            SiteConfig fileStorage = new SiteConfig
            {
                Item = "FileStorage",
                Value = "AzureFileStorage",
                Description = "LocalFileStorage 或者 AzureFileStorage.",
                ItemLabel = "存储方式",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            SiteConfig localStorageFolderPath = new SiteConfig
            {
                Item = "LocalStorageFolderPath",
                Value = System.Web.Hosting.HostingEnvironment.MapPath("~") + "Content",
                Description = "Store in the same path of WebSite",
                SystemConfig = true,
                SettingGroup = "WPN",
                ItemLabel = ""
            };
            SiteConfig localImageFolderPath = new SiteConfig
            {
                Item = "LocalImageFolderPath",
                Value = localStorageFolderPath.Value + Path.DirectorySeparatorChar + "Images",
                SystemConfig = true,
                SettingGroup = "WPN",
                Description = "The folder path for images in local",
                ItemLabel = "LocalImageFolderPath"
            };
            SiteConfig localHtmlFolderPath = new SiteConfig
            {
                Item = "LocalHtmlFolderPath",
                Value = localStorageFolderPath.Value + Path.DirectorySeparatorChar + "Html",
                SystemConfig = true,
                SettingGroup = "WPN",
                ItemLabel = "Local Html Folder Path",
                Description = "The path for local static files"
            };
            SiteConfig azureStorageFolderPath = new SiteConfig
            {
                Item = "AzureStorageFolderPath",
                Value = "https://WPNstatic.blob.core.chinacloudapi.cn/",
                Description = "Azure Blob",
                SystemConfig = true,
                SettingGroup = "WPN",
                ItemLabel = "Azure Storage Folder Path"
            };
            SiteConfig imageFolderExtensions = new SiteConfig
            {
                Item = "ImageFolderExtensions",
                Value = "jpg,jpeg,png,bmp,gif,tif",
                SystemConfig = true,
                Editable = false,
                SettingGroup = "WPN",
                Description = "What kind of images to be placed in the folder?"
                ,
                ItemLabel = "ImageFolderExtensions"
            };
            SiteConfig htmlFolderExtensions = new SiteConfig
            {
                Item = "HtmlFolderExtensions",
                Value = "html,css,htm,js,txt",
                SystemConfig = true,
                Editable = false,
                SettingGroup = "WPN",
                Description = "What kind of files could be placed in the static file folder?"
                ,
                ItemLabel = "HtmlFolderExtensions"
            };
            SiteConfig wpnApp = new SiteConfig
            {
                Item = "wpnApp",
                Value = "WPN",
                SystemConfig = true,
                Editable = false,
                SettingGroup = "WPN",
                Description = "The application's name",
                ItemLabel = "wpnApp"
            };
            SiteConfig startPage = new SiteConfig
            {
                Item = "StartPage",
                Value = "default",
                ItemLabel = "首页",
                Description = "选择默认首页或者指定页面",
                SystemConfig = true,
                SettingGroup = "WPN",
            };
            var routeConfig = new SiteConfig
            {
                Item = "routing",
                Value = "%category%/%title%",
                ItemLabel = "固定链接",
                Description = "自定义固定链接的模式",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            var routePatternConfig = new SiteConfig
            {
                Item = "url_pattern",
                Value = @"[A-Za-z0-9\-]+/[A-Za-z0-9]+",
                ItemLabel = "",
                Description = "",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            var sitePrefixConfig = new SiteConfig
            {
                Item = "site_prefix",
                Value = "http://localhost/wpn",
                ItemLabel = "Site's prefix",
                Description = "For the website",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            var themeConfig = new SiteConfig
            {
                Item = "site_theme",
                Value = "football",
                ItemLabel = "Theme of the website",
                Description = "setting the theme of the website",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            var activatedPlugins = new SiteConfig
            {
                Item = "plugins_activated",
                Value = "force_recompilation",
                ItemLabel = "Activated Plugins",
                Description = "Activated Plugins",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            var recompliation_exec = new SiteConfig
            {
                Item = "plugin_force_recompilation_exec",
                Value = "~/WPN-Content/Plugins/force_recompilation/recompilation.cshtml",
                ItemLabel = "force_recompilation",
                Description = "force_recompilation",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            //var soccer_exec = new SiteConfig {
            //    Item = "plugin_soccer_exec",
            //    Value = "~/WPN-Content/Plugins/soccer/soccer.cshtml",
            //    ItemLabel = "Soccer plugin",
            //    Description = "A plugin developed for football leagues",
            //    SystemConfig = false,
            //    SettingGroup="football"
            //};
            var language = new SiteConfig
            {
                Item = "Language",
                Value = "zh_CN",
                ItemLabel = "Language",
                Description = "Set the system language",
                SystemConfig = true,
                SettingGroup = "WPN"
            };
            wpn.DbCfg.Add(new[] {
                //siteName
                fileStorage
                ,localStorageFolderPath
                ,localImageFolderPath
                ,localHtmlFolderPath
                ,azureStorageFolderPath
                ,imageFolderExtensions
                ,htmlFolderExtensions
                ,wpnApp
                ,startPage
                ,routeConfig
                ,routePatternConfig
                ,sitePrefixConfig
                ,themeConfig
                ,activatedPlugins
                ,recompliation_exec
                //,soccer_exec
                ,language
            });




            //    RelationShip system_index = new RelationShip { TermID = system.TermID, RefID = systemPage.TaxonomyID, ValueID = index.PageID };
            //    RelationShip system_404 = new RelationShip { TermID = system.TermID, RefID = systemPage.TaxonomyID, ValueID = undefined.PageID };
            //    RelationShip england_primerLeague = new RelationShip { TermID = taxonomy.TermID, RefID = englandTaxonomy.TaxonomyID, ValueID = primerLeagueTaxonomy.TaxonomyID };
            //    RelationShip germany_bundesLeague = new RelationShip { TermID = taxonomy.TermID, RefID = germanyTaxonomy.TaxonomyID, ValueID = bundesLeagueTaxonomy.TaxonomyID };
            //    RelationShip primerLeague = new RelationShip { TermID = taxonomy.TermID, RefID = league.TaxonomyID, ValueID = primerLeagueTaxonomy.TaxonomyID };
            //    RelationShip bundesLeague = new RelationShip { TermID = taxonomy.TermID, RefID = league.TaxonomyID, ValueID = bundesLeagueTaxonomy.TaxonomyID };
            //    RelationShip italyLeague = new RelationShip { TermID = taxonomy.TermID, RefID = league.TaxonomyID, ValueID = italyLeagueTaxonomy.TaxonomyID };
            //    RelationShip primerLeague_Arsenal = new RelationShip { TermID = custom_page.TermID, RefID = primerLeagueTaxonomy.TaxonomyID, ValueID = arsenalPage.PageID };
            //    RelationShip primerLeague_MU = new RelationShip { TermID = custom_page.TermID, RefID = primerLeagueTaxonomy.TaxonomyID, ValueID = muPage.PageID };
            //    RelationShip bundesLeague_BVB = new RelationShip { TermID = custom_page.TermID, RefID = bundesLeagueTaxonomy.TaxonomyID, ValueID = bvbPage.PageID };
            //    RelationShip mu_rvp = new RelationShip { TermID = custom_page.TermID, RefID = muTeamTaxonomy.TaxonomyID, ValueID = soccer_rvp.PageID };
            //    RelationShip system_category_index = new RelationShip { TermID=system.TermID,RefID=systemPage.TaxonomyID,ValueID=categoryIndex.PageID };
            //    RelationShip.Add(new[] {
            //        system_index,
            //        system_404,
            //        england_primerLeague,
            //        germany_bundesLeague,
            //        primerLeague,
            //        bundesLeague,
            //        italyLeague,
            //        primerLeague_Arsenal,
            //        primerLeague_MU,
            //        bundesLeague_BVB,
            //        mu_rvp });
        }
    }
}
