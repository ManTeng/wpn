﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using WPN;
using WPN.Handlers;

namespace Bee.Core
{
    public class RouteData
    {
        public RouteData(string pattern)
        {
            Pattern = pattern;
            RouteNodes = new List<RouteNode>();
            GenNodes(pattern);
        }
        public IBeeHttpHandler Handler { get; set; }
        public Action<HttpContext> Execution { get; set; }
        public Func<string> Predicator { get; internal set; }
        public string Pattern { get; set; }
        public int Priority { get; set; }
        public bool ApplyAuthEvent { get; internal set; }
        public List<RouteNode> RouteNodes { get; set; }
        internal string Dir { get; set; }
        public WPNApp App { get; set; }
        void GenNodes(string pattern)
        {
            var items = pattern.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < items.Length; i++)
            {
                var node = new RouteNode(items[i]);
                RouteNodes.Add(node);
            }
        }

        public decimal Caculate(string request)
        {
            decimal score = 0;
            var segments = request.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            decimal avg = 45 / segments.Length;
            decimal weight = (RouteNodes.Count / segments.Length) * 10;
            bool stop = false;
            if (RouteNodes.Count <= segments.Length)
            {
                int p = 0;
                for (int i = 0; i < RouteNodes.Count; i++)
                {
                    if (RouteNodes[i].StringMatch(segments[p]))
                    {
                        score += avg * 2;
                        ++p;
                    }
                    else
                    {
                        score += avg;
                        segments = request.Replace(Regex.Match(request, RouteNodes[i].Data).Value, string.Empty).Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        p = 0;
                    }
                }
            }
            else
            {
                return -1;
            }
            return score + weight;
        }
    }
}
