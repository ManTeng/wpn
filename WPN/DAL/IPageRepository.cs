﻿using System;
using System.Collections.Generic;
using WPN.Models;

namespace WPN.DAL
{
    public interface IPageRepository: IRepository<Page>
    {
        IEnumerable<Page> LoadAll(int pageNum, int amount);

        IEnumerable<Tag> LoadTags(int pageID);

        IEnumerable<Taxonomy> LoadCateogories(int pageID);

        IEnumerable<PostMeta> LoadMeta(int pageID);

        IEnumerable<Page> Search(string pageTitleContains, string pageContentContains, DateTime? dateBefore, DateTime? dateAfter, string tagName, string taxonomyName, int page, bool loadMeta=false);

        bool CheckExistByName(string pageName);

        Page GetByCondition(string pageName, string author = null, string category = null, int? year = null, int? month = null, int? day = null, int? id=null);

        int Count(int pageStatus);

        bool ValidateTimeStamp(int pageid, byte[] timestamp);
    }
}
