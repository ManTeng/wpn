﻿using WPN.Models;

namespace WPN.DAL
{
    public interface IUserRepository
    {
        void Create(WPNUser user);
        void Delete(WPNUser user);
        WPNUser FindById(string userId);
        WPNUser FindByName(string userName);
        bool HasAdmin();
        void Update(WPNUser user);
        string ValidateUser(string userid, string hashedPwd);
        bool ValidateToken(string username, string token);
    }
}
