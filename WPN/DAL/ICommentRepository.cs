﻿using System.Collections.Generic;
using WPN.Models;

namespace WPN.DAL
{
    public interface ICommentRepository
    {
        void AddComment(Comment item);
        List<Comment> GetComments(int pageID, int pageNum, int count);
        void Remove(int commentID);
        void RemoveAll(int pageID);
        void MarkSpam(int commentID);
        void MarkNormal(int commentID);
    }
}
