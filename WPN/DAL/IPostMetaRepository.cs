﻿using WPN.Models;

namespace WPN.DAL
{
    public interface IPostMetaRepository : IRepository<PostMeta>
    {
    }
}
