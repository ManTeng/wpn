﻿namespace WPN.DAL
{
    public interface IDataBase
    {
        bool CreatTables(string prefix);
        bool CheckTables();
        void SetConnectionString(string connstr);
        string GetConnectionString(string datasource, string database, string user, string pwd);
    }
}
