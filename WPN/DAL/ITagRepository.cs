﻿using WPN.Models;
using System.Collections.Generic;

namespace WPN.DAL
{
    public interface ITagRepository
    {
        bool Add(Tag item);
        bool Remove(Tag item);
        bool Update(Tag item);
        bool IncRefCount(Tag item);
        bool BatchIncRefCount(IEnumerable<Tag> item);
        bool DecRefCount(Tag item);
        bool BatchDecRefCount(IEnumerable<Tag> item);
        IEnumerable<Tag> GetList(int pageID);
    }
}
