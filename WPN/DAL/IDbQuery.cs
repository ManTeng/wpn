﻿using System.Collections.Generic;

namespace WPN.DAL
{
    public interface IDbQuery
    {
        IEnumerable<T> Query<T>(string sql, params object[] parameters);
        void Execute(string sql, params object[] parameters);
    }
}
