﻿using System.Collections.Generic;
using WPN.Models;

namespace WPN.DAL
{
    public interface ISiteConfigRepository : IRepository<SiteConfig>
    {
        SiteConfig GetConfig(string index);
        SiteConfig GetConfig(string index, string group);
        List<SiteConfig> LoadConfigs();
        bool IsExist(string index);
    }
}
