﻿using WPN.Models;

namespace WPN.DAL
{
    public interface IRelationShipRepository
    {
        bool Add(RelationShip item);
        bool Add(RelationShip[] items);
        bool Remove(RelationShip item);
        void RemoveTermRelationShip(string customPage, bool isRefID, int pageID);
    }
}
