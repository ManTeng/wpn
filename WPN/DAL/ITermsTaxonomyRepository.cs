﻿using System.Collections.Generic;
using WPN.Models;

namespace WPN.DAL
{
    public interface ITaxonomyRepository : IRepository<Taxonomy>
    {
        IEnumerable<Taxonomy> LoadLeagues();

        bool Exist(string taxonomyName);

        bool UrlAbbrExist(string urlAbbr);
        int GetID(int termId, string taxonomyName);
        IEnumerable<Taxonomy> GetChildren(int taxonomyId);
        IEnumerable<Taxonomy> GetByTerm(string termName);
    }
}