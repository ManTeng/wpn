﻿using WPN.Models;

namespace WPN.DAL
{
    public interface ITermRepository : IRepository<Term>
    {
        int GetID(string termName);
    }
}