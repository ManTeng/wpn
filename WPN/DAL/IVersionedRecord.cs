﻿namespace WPN.DAL
{
    interface IVersionedRecord<T1>
    {
        bool ValidateVersion(T1 key, byte[] timestamp);
    }
}
