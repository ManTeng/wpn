﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using Bee.Core;

namespace WPN
{
    public class UrlRoutingModule : IHttpModule
    {
        private HashSet<string> skippedExt = null;

        public UrlRoutingModule()
        {
            skippedExt = new HashSet<string> {
                ".png", ".css", ".jpg", ".jpeg",".exe",".dll",".7z",".tar",".c",".cpp",".html",".htm",".xml",".json",".less",
                    ".gif", ".tiff", ".bmp", ".js",
                    ".icon", ".ico", ".pdf", ".txt",
                    ".tar", ".zip", ".rar", ".doc",
                    ".xlsx", ".docx", ".xls", ".ppt",
                    ".pptx",".woff",".ttf", ".woff2",".min.js",".min.css"
            };
        }

        public void Dispose()
        { }

        public void Init(HttpApplication app)
        {
            
            app.BeginRequest += Context_BeginRequest;
            app.PostResolveRequestCache += Context_PostResolveRequestCache;
        }


        private void Context_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;
            var ext = Path.GetExtension(context.Request.Url.PathAndQuery);
#if DEBUG
            Debug.WriteLine(ext + " -- " + context.Request.Url.PathAndQuery);
#endif
            if (ext != null && skippedExt.Contains(ext))
            {
                return;
            }

            EventAction.Apply("Context_BeginRequest", context);

            EventAction.Apply("Request_HandleRequest", context);

        }

        private void Context_PostResolveRequestCache(object sender, EventArgs e)
        {
        }
    }
}
