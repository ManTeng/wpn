﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Bee.Core
{
    internal static class BuilderLog
    {
        static string temp = AppDomain.CurrentDomain.BaseDirectory;
        static string buildDIR = temp + "BeeBuilder";
        static string buildrecord = buildDIR + Path.DirectorySeparatorChar + "buildlog.dat";
        static Dictionary<string, BuildRecord> cache = new Dictionary<string, BuildRecord>();

        static readonly object _lock = new object();

        internal static  void Write(string fileName,DateTime lastWriteTime, string asmName, string pathKey)
        {
            var fn = fileName.ToLower();
            if (cache.ContainsKey(fn))
            {
                cache[fn].LWTime = lastWriteTime.GetUnixTimeStamp();
            }
            else
            {
                cache[fn] = new BuildRecord { LWTime = lastWriteTime.GetUnixTimeStamp(), asmName = asmName, namehash = fn.GetHashCode(), PathKey = pathKey };
            }
        }

        internal static bool ShouldCompile(FileInfo[] fileList, string pathKey, out string asm)
        {
            bool goCompile = false;
            string libName = null;
            foreach (var item in fileList)
            {
                if (ShouldCompile(item.FullName, item.LastWriteTimeUtc, pathKey, out libName))
                {
                    goCompile = true;
                }
            }
            asm = libName;
            return goCompile;
        }

        internal static bool ShouldCompile(string filename, DateTime lastWriteTime, string pathKey, out string asm)
        {
            if (cache.Count == 0)
            {
                asm = null;
                return true;
            }
            var fn = filename.ToLower();
            if (!cache.ContainsKey(fn))
            {
                asm = null;
                return true;
            }
            var br = cache[fn];
            bool p = false;
            if (cache.ContainsKey(fn))
            {
                if (br.LWTime == lastWriteTime.GetUnixTimeStamp() && br.PathKey==pathKey)
                {
                    p = false;
                }
                else
                {
                    p = true;
                }
            }
            asm = br.asmName;
            return p;
        }
    }



    class BuildRecord
    {
        public int namehash;
        public string asmName;
        public uint LWTime;
        public string PathKey { get; set; }
    }
}
