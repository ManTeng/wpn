﻿using Bee.Core;
using System;
using System.Collections.Generic;
using System.IO;
using WPN.DAL;
using WPN.Models;

namespace WPN
{
    /// <summary>
    /// This class is only for the site
    /// </summary>
    public class MemoryCfg
    {
        private const string connStrSetting = "connStr";
        WPNApp wpn = null;
        AppService app = null;
        public int InstallStep { get; set; }

        public string TablePrefix { get { return Get("TablePrefix"); } }

        internal readonly string DefaultLanguage = "en-us";

        public string Language
        {
            get
            {
                string val = Get("Language");

                return val == null ? DefaultLanguage : val;
            }
            set
            {
                var oldLanguage = Language;
                Set("Language", value);
                EventAction.Apply("update_language", oldLanguage, value);
            }
        }

        public  string AdminFolder
        {
            get
            {
                var val= Get("AdminFolder");
                if (val==null)
                {
                    val = "~/wpnAdmin/";
                    Set("AdminFolder", val);
                }
                return val;
            }
            set
            {
                Set("AdminFolder", value);
            }
        }


        public  string ConnectionString
        {
            get { return Get(connStrSetting); }
            set
            {
                if (value != null)
                {
                    Set(connStrSetting, value);
                }
            }
        }

        public MemoryCfg(WPNApp wpn)
        {
            this.wpn = wpn;
            app = wpn.App;
        }

        public  bool IsInstalled()
        {
            if (Get("Installed") == "true")
            { return true; }
            if (!EventFilter.Has("IOC_GetObject"))
            {
                InstallStep = Int32.MinValue;
                return false;
            }
            if (ConnectionString == null)
            {
                Set("Installed", "false");
                return false;
            }
            var type = ResolveService.Create<IDataBase>();
            if (type.CheckTables())
            {
                if (wpn.DbCfg.SiteName == null)
                {
                    InstallStep = 3;
                }
                else
                {
                    if (wpn.Users.HasAdmin())
                    {
                        Set("Installed", "true");
                        InstallStep = Int32.MaxValue;
                        return true;
                    }
                    else
                    {
                        InstallStep = 4;
                    }
                }
            }
            else
            {
                InstallStep = 2;
            }
            Set("Installed", "false");

            return false;
        }

        public void Set(string key, string v)
        {
            app.Vars.Set(key, v);
        }

        public string Get(string key)
        {
            return app.Vars.Get(key);
        }

        public  void RefreshConfig()
        {
            RefreshConfigIntenal();
        }

        internal void RefreshConfigIntenal()
        {
            var configFile = Utl.GetAbsPath("~/WPNConfig.cshtml");
            var output = "@using Bee.Core;" +  Environment.NewLine;
            output += "@using WPN;" + Environment.NewLine;
            output += "@functions {" + Environment.NewLine + "\tpublic void Config(WPNApp app) {" + Environment.NewLine;
            var settings = wpn.App.Vars.Settings;
            foreach (var item in settings.Keys)
            {
                    output += "\t\tapp.FileCfg.Set(\"" + item + "\",@\"" + settings[item] + "\");" + Environment.NewLine;
            }
            output += "\t\tEventAction.Apply(\"WPN_Config\", app);" + Environment.NewLine;
            output += "\t}" + Environment.NewLine + "}";
            File.WriteAllText(configFile, output, System.Text.Encoding.UTF8);
            FileInfo fi = new FileInfo(System.Web.Hosting.HostingEnvironment.MapPath( Path.DirectorySeparatorChar +  "Web.config"));
            fi.LastWriteTime = DateTime.Now;
        }

        
    }
}
