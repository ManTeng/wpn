﻿using System.Text.RegularExpressions;

namespace Bee.Core
{
    public class RouteNode
    {
        public string Data { get; set; }
        public bool Optional { get; set; }

        public RouteNode(string data)
        {
            Data = data;
        }

        /*
        Route Pattern
            /wpnAdmin/[a-zA-Z0-9]/List/[a-z]{1,9}
            [a-z]{1,9} is optional
        Authorization
            WPNUser.Validate
        Structure
            Root -> wpnAdmin -> [a-zA-Z0-9] -> List -> [a-z]{1,9}
        Test
            /wpnAdmin
                Not matched
            /wpnAdmin/test
                Not matched
            /wpnAdmin/test/List
                
        */

        public bool StringMatch(string pattern)
        {
            return Data == pattern ;
        }

        public bool RegExMatch(string pattern)
        {
            return Regex.Match(pattern, Data).Success;
        }
    }
}
