﻿namespace Bee.Core
{
    public interface IPlugin
    {
        void Activate(AppService app);
        void DeActivate(AppService app);
    }
}
