﻿using System;
using System.IO;
using System.Web;

namespace Bee.Core
{
    public static class Utl
    {
        internal static char SEP = char.MinValue;

        public static string SystemDLLPath = typeof(object).Assembly.Location.Replace("mscorlib.dll", string.Empty);

        public static string GetAbsPath(string path)
        {
            if (path[0] == '~' && path[1] == '/')
            {
                return HttpRuntime.AppDomainAppPath + path.Substring(2).Replace(SEP,Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
            }
            return path;
        }

        public static bool HasMethod(this Type type, string methodName)
        {
            return type.GetMethod(methodName) != null;
        }

        public static bool HasProperty(this Type type, string prop)
        {
            return type.GetProperty(prop) != null;
        }

        public static string Hash(this string input)
        {
            string hash = null;
            hash = EventFilter.Apply<string>("Hash_String", hash, input);
            return hash;
        }

        public static uint GetUnixTimeStamp(this DateTime datetime)
        {
            return Convert.ToUInt32(datetime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
        }

        public static DateTime ToDateTime(this uint unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(unixTimeStamp);
        }

        public static bool IsWindows()
        {
            var p = (int)Environment.OSVersion.Platform;
            return (p != 4) && (p != 6) && (p != 128);
        }

        public static char GetSEP()
        {
            if (SEP==char.MinValue)
            {
                SEP = IsWindows() ? '\\' : '/';
            }
            return SEP;
        }
    }
}