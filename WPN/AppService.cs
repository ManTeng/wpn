﻿
using System;

namespace Bee.Core
{
    public class AppService
    {
        public PluginManager Plugins { get; set; }
        public VarManagement Vars { get; set; }



        public AppService() {
            
            Plugins = new PluginManager(this);
            Vars = new VarManagement();

            InitVar();
        }

        void InitVar()
        {
            Utl.GetSEP();
            EventAction.Apply("AppService_INITVAR",this);
        }

        public void Use(string pluginName)
        {
            var pn = Plugins.SantilizePluginName(pluginName);
            Plugins.Active(pluginName);
        }

    }
}
