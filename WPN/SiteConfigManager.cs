﻿using System;
using System.Collections.Generic;
using WPN;
using WPN.DAL;
using WPN.Models;

namespace Bee.Core
{
    public class SiteConfigManager
    {
        private Dictionary<string, SiteConfig> _loadedConfigs = new Dictionary<string, SiteConfig>();
        private readonly object _lock = new object();

        public void LoadConfigs()
        {
            List<SiteConfig> configs = ResolveService.Create<ISiteConfigRepository>().LoadConfigs();
            configs.ForEach(x => { _loadedConfigs[x.Item] = x; });
        }

        public void RefreshConfigs()
        {
            lock (_lock)
            {
                _loadedConfigs.Clear();
                LoadConfigs();
            }
        }

        public SiteConfig GetConfig(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (!_loadedConfigs.ContainsKey(key))
            {
                lock (_lock)
                {
                    if (!_loadedConfigs.ContainsKey(key))
                    {
                        var config = ResolveService.Create<ISiteConfigRepository>().GetConfig(key);
                        _loadedConfigs[key] = config;
                    }
                }
            }
            return _loadedConfigs[key];
        }

        public void RemoveCfg(string key)
        {
            if (_loadedConfigs.ContainsKey(key))
            {
                lock (_lock)
                {
                    _loadedConfigs.Remove(key);
                }
            }
            ResolveService.Create<ISiteConfigRepository>().Remove(new SiteConfig { Item = key });
        }

        public void SetConfig(string key, string v, string itemLabel = "empty", string desc = "Config", bool editable = true, string settingGroup = "custom", bool systemConfig = false)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }
            if (v == null)
            {
                throw new ArgumentNullException("v");
            }
            var repository = ResolveService.Create<ISiteConfigRepository>();
            var config = repository.GetConfig(key);
            if (config != null)
            {
                config.Value = v;
                repository.Update(config);
            }
            else
            {
                config = new SiteConfig { Item = key, Value = v, Description = desc, Editable = editable, ItemLabel = itemLabel, SettingGroup = settingGroup, SystemConfig = systemConfig };
                repository.Add(config);
            }
            lock (_lock)
            {
                _loadedConfigs[key] = config;
            }
        }


        public string GetVal(string key)
        {
            var config = GetConfig(key);
            if (config != null)
            {
                return config.Value;
            }
            return null;
        }

        public string ImageFolderExtensions
        {
            get
            {
                return GetVal("ImageFolderExtensions");
            }
        }
        public string HtmlFolderExtensions
        {
            get
            {
                return GetVal("HtmlFolderExtensions");
            }
        }

        public string SiteName
        {
            get { return GetVal("sitename"); }
            set { SetConfig("sitename", value); }
        }

        internal void Add(SiteConfig[] siteConfigs)
        {
            ResolveService.Create<ISiteConfigRepository>().Add(siteConfigs);
        }

        public string UrlPattern
        {
            get { return GetVal("url_pattern"); }
        }

        public string SitePrefix
        {
            get { return GetVal("site_prefix"); }
        }

        public string Routing
        {
            get { return GetVal("routing"); }
            set { SetConfig("routing", value); }
        }

        public string SiteTheme
        {
            get { return GetVal("site_theme"); }
            set { SetConfig("site_theme", value); }
        }


        public string[] ActivatedPlugins()
        {
            var str = GetVal("plugins_activated");
            return str == null ? null : str.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public string GetPluginExecutor(string computedResult, string pluginName)
        {
            return GetVal("plugin_" + pluginName + "_exec");
        }
    }
}
