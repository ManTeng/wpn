﻿using System.Collections.Generic;
using WPN.Models;
using System;

namespace WPN
{
    public class WPNMenu
    {
        public static IEnumerable<Taxonomy> GetMenu(string menuName )
        {
            return Term.GetTaxonomies(Term.Menu, menuName);
        }

        /// <summary>
        /// Return a javascript object which contains the menu
        /// </summary>
        /// <returns></returns>
        //public static string GetPrimaryMenu()
        //{
        //    var list = Term.GetTaxonomies(Term.Menu, SiteConfig.GetConfig("primary_menu_name"));
        //    if (list==null)
        //    {
        //        return null;
        //    }
        //    throw new NotImplementedException();
        //}
    }
}
