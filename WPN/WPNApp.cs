﻿using Bee.Core;
using Bee.Core.Compilers;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using WPN.Models;

namespace WPN
{
    public class WPNApp : IPlugin
    {
        const string PluginName = "WPN";
        public AppService App { get;  private set; }
        public SiteConfigManager DbCfg { get; private set; }
        public MemoryCfg FileCfg { get; private set; }
        public DBPageManager DbPages { get; private set; }
        public ThemeManager Theme { get; private set; }
        public UserManager Users { get; private set; }
        public Language Lang { get; set; }
        internal Route Routes { get; set; }
        public HandlerFactory HanderFactory { get; private set; }

        public WPNApp()
        {
            var wpnInstance = this;
            App = new AppService();
            DbCfg = new SiteConfigManager();
            FileCfg = new MemoryCfg(this);
            DbPages = new DBPageManager(this);
            Theme = new ThemeManager(this);
            Users = new UserManager(this);
            Lang = new Language("WPN");
            Routes = new Route();
            HanderFactory = new HandlerFactory(this);
            HanderFactory.RegisterHandler("WebPage", typeof(Handlers.PageHandler));
            HanderFactory.RegisterHandler("MVC", typeof(Handlers.MVCHandler));
            HanderFactory.RegisterHandler("API", typeof(Handlers.APIHandler));
            App.Plugins.PluginFolderPath = "~/WPN-Content/Plugins/";
            App.Plugins.AbsPluginFolderPath = Utl.GetAbsPath(App.Plugins.PluginFolderPath);

        }


        public void Activate(AppService app)
        {
            EventFilter.Add("WPN_Instance", new Func<WPNApp, string,WPNApp>((x,y) => { return this; }));
            EventAction.Add("CoreSrv_AppStart", new Action(RunConfig));
            EventAction.Add("Plugin_Run_AppStart", new Action(LoadSettings));
            EventAction.Add("WPN_INSTALLED", new Action(DefaultInstalledAction));
            EventFilter.Add("Plugin_GetActived", new Func<IEnumerable<string>>(this.DbCfg.ActivatedPlugins));
            EventFilter.Add("Plugin_GetExecutor", new Func<string, string, string>(this.DbCfg.GetPluginExecutor));
            EventFilter.Add("User_Authentication", new Func<bool, HttpCookieCollection, bool>(Users.DefaultAuthenticationhandler));
            EventAction.Add("User_Authentication_Failed", new Action(User_Authentication_Failed));
            EventFilter.Add("Admin_Request_Handler", new Func<string>(GetAdminPage));
            EventFilter.Add("Hash_String", new Func<string, string, string>(EncryptString));
            EventAction.Add("Context_Error", new Action<Exception,string>(DisplayError));
            EventAction.Add("WPN_ERROR_SOURCE_ERRLIST", new Action<string, IEnumerable<string>>(DisplayErrorMessages));
            EventAction.Add("Compile", new Func<CompileResult, ProjectJson, List<MetadataReference>, string[], CompileResult>((rawResult, prj, extraReference, source) =>
            {
                //ProjectDef.Including.ForEach(x => including.Add(PluginPath + Utl.SEP + x));
                var csCompiler = new RoslynCompiler();
                if (extraReference!=null)
                {
                    csCompiler.SelfReference.AddRange(extraReference);
                }
                return csCompiler.Compile(source);
            }));
            EventAction.Apply("WPN_Add_Custom_Filter");
        }

        void User_Authentication_Failed()
        {
            HttpContext.Current.Response.Redirect("/wpnAdmin/Login");
        }

        void LoadSettings()
        {
            this.Route("/wpnAdmin/Install", 1).PageHandler();
            this.Route(RouterHelper.TranslateUrl("/wpnAdmin(/{*query})?"), 1).PageHandler(new Func<string>(AdminRequest)).Authorized();
            if (FileCfg.Language != FileCfg.DefaultLanguage)
            {
                var loaded = Lang.AddTranslation(FileCfg.Language, Utl.GetAbsPath("~/WPN-Content/language/" + FileCfg.Language + ".po"));
                if (!loaded)
                {
                    FileCfg.Language = FileCfg.DefaultLanguage;
                }

            }

            if (!FileCfg.IsInstalled())
            {
                this.Route("/").Redirect("/wpnAdmin/Install?step=" + FileCfg.InstallStep);
                //Error(T(""));
            }
            else
            {
                EventAction.Apply("WPN_INSTALLED");
            }
        }

        void DefaultInstalledAction()
        {
            var wpnUrlPattern = DbCfg.UrlPattern;
            this.Route(@"/.*", 1).PageHandler(new Func<string>(GetPage));
            App.Plugins.LoadRequestPlugins();
            Theme.Active(DbCfg.SiteTheme);
        }
        

        string T(string t)
        {
            var tl = Lang.GetTranslator(FileCfg.Language);
            if (tl == null) return t;
            return tl.T(t);
        }

        void RunConfig()
        {
            dynamic config = BeeBuilder.BuildRazorPage(new string[] { "~/WPNConfig.cshtml" });
            config.Config(this);
            
        }

        string GetPage()
        {
            var request = HttpContext.Current.Request;
            var items = request.Url.PathAndQuery.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            string page = null;
            string themeDir = Theme.ThemeDir;

            var routingFormat = DbCfg.Routing.Replace("%", string.Empty);
            var expected_tags = routingFormat.Split('/');
            if (items.Length == 0)
            {
                page = themeDir + "index.cshtml";
            }
            if (items.Length == 1)
            {
                if (Taxonomy.UrlAbbrExist(items[0]))
                {
                    page = themeDir + "Category.cshtml";
                }
                if (expected_tags[0] == "id")
                {
                    page = themeDir + "Page.cshtml";
                }
                if (expected_tags[0] == "title")
                {
                    page = themeDir + "Page.cshtml";
                }
            }
            if (EventFilter.Has("WPN_Request_Handler"))
            {
                page = EventFilter.Apply<string>("WPN_Request_Handler", page, request.Url.PathAndQuery);
            }
            if (page == null || page.Trim()==string.Empty)
            {
                page = themeDir + "404.cshtml";
            }
            return page;
        }

        string AdminRequest()
        {
            var context = HttpContext.Current;
            WPNUser wpnUser = null;
            var page = EventFilter.Apply<string>("Admin_Request_Handler");

            if (!FileCfg.IsInstalled() && page != FileCfg.AdminFolder + "Install.cshtml")
            {
                page = FileCfg.AdminFolder + "Install.cshtml";
            }
            if (page == null || page.Trim() == string.Empty)
            {
                page = FileCfg.AdminFolder + "Index.cshtml";
            }
            if (page != FileCfg.AdminFolder + "Install.cshtml" && page != FileCfg.AdminFolder + "Login.cshtml")
            {
                wpnUser = Users.FindByName(context.Request.Cookies[DbCfg.SiteName + "_token"].Value.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[0]);
                HttpContext.Current.Items["WPN_USER"] = wpnUser;

            }
            return page;
        }

        string GetAdminPage()
        {
            var query = HttpContext.Current.Request.Url.PathAndQuery;
            string result = string.Empty;
            var items = query.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var wpnAdminFolder = FileCfg.AdminFolder;
            if (items.Length == 1)
            {
                return wpnAdminFolder + "Index.cshtml";
            }
            var action = items[1];
            int pos = items[1].IndexOf('?');
            if (pos>0)
            {
                action = items[1].Substring(0, pos);
            }
            switch (action)
            {
                case "Dashboard":
                    return wpnAdminFolder + "Index.cshtml";
                case "Login":
                    result = wpnAdminFolder + "Login.cshtml";
                    break;
                case "Media":
                    result = wpnAdminFolder + "Media.cshtml";
                    break;
                case "Pages":
                    result = wpnAdminFolder + "Pages.cshtml";
                    break;
                case "Plugins":
                    result = wpnAdminFolder + "Plugins.cshtml";
                    break;
                case "Profile":
                    result = wpnAdminFolder + "Profile.cshtml";
                    break;
                case "Settings":
                    result = wpnAdminFolder + "Setting.cshtml";
                    break;
                case "Themes":
                    result = wpnAdminFolder + "Themes.cshtml";
                    break;
                default:
                    result = wpnAdminFolder + "Admin404.cshtml";
                    break;
            }
            return result;
        }

        public string EncryptString(string computedString, string input)
        {
            SHA256Managed crypt = new SHA256Managed();
            string hash = string.Empty;
            var bytes = Encoding.UTF8.GetBytes(input + FileCfg.Get("Hash_Salt"));
            byte[] hashBytes = crypt.ComputeHash(bytes);
            foreach (byte item in hashBytes)
            {
                hash += item.ToString("x2");
            }
            return hash;
        }

        public void DeActivate(AppService app)
        {
            throw new NotImplementedException();
        }

        public void DisplayError(Exception e, string source)
        {
            var response = HttpContext.Current.Response;
            response.Write("<span>" + source + "</span>");
            response.Write("<p>" + e.Message + "</p>");
            response.Write("<p>" + e.StackTrace + "</p>");
            if (e.InnerException != null)
            {
                DisplayError(e, source);
            }
            response.End();
        }

        public void DisplayErrorMessages(string source, IEnumerable<string> msgs)
        {
            var response = HttpContext.Current.Response;
            response.Write("<span>" + source + "</span>");
            response.Write("<ol>");
            foreach (var msg in msgs)
            {
                response.Write("<li>" + msg + "</msg>");
            }
            response.Write("</ol>");
            response.End();
        }
    }
}
