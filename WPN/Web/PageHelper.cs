﻿using Bee.Core;
using System.Web.WebPages;

namespace WPN
{
    public static class PageHelper
    {
        public static string WPN_T(this WebPage page, string text)
        {
            var wpn = EventFilter.Apply<WPNApp>("WPN_Instance", null, null);
            if (page.VirtualPath.StartsWith(wpn.FileCfg.AdminFolder))
            {
                if (wpn.FileCfg.Language=="en-us")
                {
                    return text;
                }
                return wpn.Lang.GetTranslator(wpn.FileCfg.Language).T(text);
            }
            return null;
        }


    }
}
