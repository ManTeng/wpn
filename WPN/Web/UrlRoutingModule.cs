﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace WPN.Web
{
    public class UrlRoutingModule : IHttpModule
    {
        static HashSet<string> badExt = new HashSet<string>();

        static UrlRoutingModule()
        {
            string[] ignoreExts = {
                    ".png", ".css", ".jpg", ".jpeg",
                    ".gif", ".tiff", ".bmp", ".js",
                    ".icon", "ico", ".pdf", ".txt",
                    ".tar", ".zip", ".rar", ".doc",
                    "xlsx", ".docx", ".xls", ".ppt",
                    ".pptx",".woff",".ttf", ".woff2",".min.js",".min.css" };
            foreach (var item in ignoreExts)
            {
                badExt.Add(item);
            }
        }

        public static HashSet<string> IgnoredExts { get { return badExt; } }

        public void Dispose()
        { }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
            context.PostResolveRequestCache += Context_PostResolveRequestCache;
        }

        private void Context_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;
            var ext = context.Request.CurrentExecutionFilePathExtension;
            if (ext!=string.Empty &&badExt.Contains(ext))
            {
                return;
            }


#if DEBUG
            if (!context.Request.Url.PathAndQuery.Contains("browser"))
            {
#endif
                //var ignored = Filter.Apply<bool>("ignoed_extenstion", false, lastSegment);
                Debug.WriteLine("Check Url");

                if (!context.Request.Url.PathAndQuery.StartsWith("/wpnAdmin/Install.cshtml"))
                {
                    WPNPlugin.RunPlugins(context);
                }
                if (context.Request.Url.PathAndQuery.StartsWith("/wpnAdmin"))
                {
                    context.RemapHandler(new AuthrizedWPNHandler(context));
                }
                else
                {
                    if (!WPNManagement.IsInstalled())
                    {
                        context.Response.Redirect("~/wpnAdmin/Install.cshtml?step=" + WPNManagement.InstallStep);
                    }
                    context.RemapHandler(new WPNHandler(context));
                }
#if DEBUG
            }
#endif
        }

        private void Context_PostResolveRequestCache(object sender, EventArgs e)
        {
            WPNAction.Apply("ResovleRequestCache", sender, e);
        }

        public static bool DefaultIgnoredExts(bool oldResult, string lastSegment)
        {
            
            var match = Regex.Match(lastSegment, @"\.{1}(.*)\?{0,1}.*");
            if (match.Success)
            {
                if (IgnoredExts.Contains(match.Value))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
