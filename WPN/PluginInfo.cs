﻿using System.Collections.Generic;

namespace Bee.Core
{
    public class PluginInfo
    {
        public string ID { get; private set; }
        public string PluginName { get; set; }
        public int MajorVersion { get; set; }
        public int MinorVersion { get; set; }
        public string Author { get; set; }
        public string PluginWebSite { get; set; }
        public string Desc { get; set; }
        public string PluginFolder { get; set; }
        public string PluginAssembly { get; internal set; }
        public string PluginExecutor { get; internal set; }
        public bool AssemblyReferenced { get; set; }
        internal readonly object PluginLock = new object();

        //public object Instance { get; set; }
        //public Dictionary<string, dynamic> Libs { get; set; }
        public Language Translator { get; set; }
        public AppService App { get; set; }
        
    }
}