﻿using System.Web;
namespace WPN
{
    public static class Helper
    {
        public static string GetAction(string prefix)
        {
            return HttpContext.Current.Request.Url.AbsolutePath.Replace(prefix, string.Empty).Trim();
        }

        public static string Request(string requestItem)
        {
            var request = HttpContext.Current.Request;
            if (request[requestItem]!=null)
            {
                return request[requestItem].Trim();
            }
            return null;
        }

        public static bool IsNullOrEmpty(this string item)
        {
            if (item==null)
            {
                return true;
            }
            if (item.Trim()==string.Empty)
            {
                return true;
            }

            return false;
        }
    }

}
