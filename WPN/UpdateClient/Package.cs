﻿using Bee.Core;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace WPN.UpdateManager
{
    public class Package
    {
        private string apiHost = null;
        private string PackageUserAgent = "";

        public Package()
        {
            apiHost = EventFilter.Apply<string>("WPN_Package_Host");
        }

        public IEnumerable<PackageInfo> GetPackagess(int pageNum = 0)
        {
            var request = WebRequest.CreateHttp(apiHost + "/app/" + pageNum) as HttpWebRequest;
            HttpWebResponse response = null;
            request.Method = "GET";
            request.UserAgent = "WPN Package Browser ";
            try
            {
                response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var stream = response.GetResponseStream();
                    var sr = new StreamReader(stream);
                    var content = sr.ReadToEnd();
                    sr.Close();
                    return Parser.Parse<IEnumerable<PackageInfo>>(content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                if (response!=null)
                {
                    response.Dispose();
                }
            }
        }

        public IEnumerable<PackageInfo> GetPackages(string[] packages)
        {
            var request = WebRequest.CreateHttp(apiHost + "/app/query?apps=" + string.Join(",", packages)) as HttpWebRequest;
            HttpWebResponse response = null;
            request.Method = "GET";
            request.UserAgent = "WPN Package Browser ";
            try
            {
                response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var stream = response.GetResponseStream();
                    var sr = new StreamReader(stream);
                    var content = sr.ReadToEnd();
                    sr.Close();
                    return Parser.Parse<IEnumerable<PackageInfo>>(content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                if (response != null)
                {
                    response.Dispose();
                }
            }
        }
    }
}
