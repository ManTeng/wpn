﻿namespace WPN.UpdateManager
{
    public class PackageInfo
    {
        public int MajorVersion { get; set; }
        public int MiniorVersion { get; set; }
        public string Version { get; set; }
        public string PackageName { get; set; }
        public string Dependency { get; set; }
        public string ReleasDate { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string WebSite { get; set; }
    }
}
