﻿using Bee.Core;
using System;
using System.Web;
using WPN.DAL;
using WPN.Models;

namespace WPN
{
    public class UserManager
    {
        public WPNApp WPN { get; set; }

         IUserRepository _repository = null;

        public UserManager(WPNApp wpn)
        {
            WPN = wpn;
        }
        public IUserRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = ResolveService.Create<IUserRepository>();
                }
                return _repository;
            }
        }

        public void CreateAdmin(WPNUser user)
        {
            user.UserType = 1;
            Create(user);
        }

        public void CreateUser(WPNUser user)
        {
            user.UserType = 2;
            Create(user);
        }

        public void CreateReader(WPNUser user)
        {
            user.UserType = 3;
            Create(user);
        }

        void Create(WPNUser user)
        {
            if (user.CheckProperty())
            {
                user.UserPwd = EventFilter.Apply<string>("Hash_String", user.UserPwd, user.UserPwd);
                user.UserToken = EventFilter.Apply<string>("Hash_String", user.UserName + WPN.FileCfg.Get("Hash_Salt") + user.UserPwd, user.UserPwd);
                Repository.Create(user);
            }
        }

        public void Delete(WPNUser user)
        {
            throw new NotImplementedException();
        }

        public WPNUser FindById(string userId)
        {
            return Repository.FindById(userId);
        }

        public WPNUser FindByName(string userName)
        {
            return Repository.FindByName(userName);
        }

        public WPNUser Current
        {
            get
            {
                WPNUser wpnUser = null;
                bool loginPassed = EventFilter.Apply<bool>("User_Authentication", false, HttpContext.Current.Request.Cookies);
                if (loginPassed)
                {
                    wpnUser = FindByName(HttpContext.Current.Request.Cookies[WPN.DbCfg.SiteName + "_token"].Value.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[0]);
                }
                return wpnUser;
            }
        }

        public string ValidateUser(string userid, string pwd)
        {
            var hashedPwd = EventFilter.Apply<string>("Hash_String", null, pwd);
            return Repository.ValidateUser(userid, hashedPwd);
        }

        public bool DefaultAuthenticationhandler(bool result, HttpCookieCollection cookie)
        {
            var request = HttpContext.Current.Request.Path;
            if (request == "/wpnAdmin/Login")
            {
                return true;
            }
            var cookies = cookie[WPN.DbCfg.SiteName + "_token"];
            if (cookies == null)
            {
                return false;
            }
            var token = cookies.Value;
            if (token == null || token.Trim() == string.Empty)
            {
                return false;
            }
            var items = token.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

            if (items.Length != 2)
            {
                return false;
            }

            return ValidateToken(items[0], items[1]);
        }

        public bool IsAuthenticated(string userid, string token)
        {
            return false;
        }

        public bool HasPrivilege(string privilege)
        {
            return false;
        }

        public bool HasAdmin()
        {
            return Repository.HasAdmin();
        }

        internal bool ValidateToken(string username, string token)
        {
            if (username.Trim() == string.Empty || token.Trim() == string.Empty)
            {
                return false;
            }
            return Repository.ValidateToken(username, token);
        }
    }
}
