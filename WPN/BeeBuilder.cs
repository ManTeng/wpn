﻿using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using System.Reflection;
using System.Web.WebPages;

namespace Bee.Core
{
    public class BeeBuilder
    {
        public static WebPage BuildRazorPage(IEnumerable<string> cshtml, IEnumerable<MetadataReference> references=null, string asmName=null)
        {
            var result = Compilers.RoslynCompiler.CompileRazorPage(cshtml, references, asmName);
            if (result.Success)
            {
                var asm = Assembly.LoadFile(result.BinaryPath);
                var page = asm.CreateInstance(asm.GetTypes()[0].FullName) as WebPage;
                return page;
            }
            return null;
        }
    }
}
