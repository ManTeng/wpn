﻿using System.Collections.Generic;

namespace Bee.Core
{
    public class ProjectJson
    {
        public References References { get; set; }
        public PluginInfo PluginInfo { get; set; }
        public List<string> Including { get; set; }

        public ProjectJson Clone()
        {
            var clone = new ProjectJson();
            if (References!=null)
            {
                clone.References = new References();
                if (References.Custom!=null && References.Custom.Count > 0)
                {
                    clone.References.Custom = new List<string>(References.Custom);
                }
                if (References.System != null && References.System.Count > 0)
                {
                    clone.References.System = new List<string>(References.System);
                }
            }
            if (Including!=null && Including.Count > 0)
            {
                clone.Including = new List<string>(Including);
            }
            clone.PluginInfo = new Core.PluginInfo();
            var cpi = clone.PluginInfo;
            var pi = PluginInfo;
            if (!string.IsNullOrEmpty(pi.PluginAssembly))
            {
                cpi.PluginAssembly = string.Copy(pi.PluginAssembly);
            }
            if (!string.IsNullOrEmpty(pi.Author))
            {
                cpi.PluginAssembly = string.Copy(pi.Author);
            }
            if (!string.IsNullOrEmpty(pi.PluginExecutor))
            {
                cpi.PluginAssembly = string.Copy(pi.PluginExecutor);
            }
            if (!string.IsNullOrEmpty(pi.PluginName))
            {
                cpi.PluginAssembly = string.Copy(pi.PluginName);
            }
            if (!string.IsNullOrEmpty(pi.Desc))
            {
                cpi.PluginAssembly = string.Copy(pi.Desc);
            }
            if (!string.IsNullOrEmpty(pi.PluginWebSite))
            {
                cpi.PluginAssembly = string.Copy(pi.PluginWebSite);
            }
            return clone;
        }
    }

    public class References
    {
        public List<string> System { get; set; }
        public List<string> Custom { get; set; }
    }
}
