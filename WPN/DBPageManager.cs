﻿using Bee.Core;
using System;
using System.Collections.Generic;
using System.Transactions;
using WPN.DAL;
using WPN.Models;

namespace WPN
{
    public class DBPageManager
    {
        public WPNApp WPN { get; set; }
        IPageRepository _repository = null;
        private readonly object _lock = new object();


        IPageRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    lock (_lock)
                    {
                        if (_repository==null)
                        {
                            _repository = ResolveService.Create<IPageRepository>();
                        }
                    }
                }
                return _repository;
            }
        }

        public DBPageManager(WPNApp wpn)
        {
            WPN = wpn;
        }

        public bool Add(Page[] pages)
        {
            return Repository.Add(pages);
        }

        public bool Delete(Page page)
        {
            bool result = false;
            if (page.PageID > 0 && !page.Deleted)
            {
                using (var ts = new TransactionScope())
                {
                    if (Repository.ValidateTimeStamp(page.PageID, page.PageTimeStamp))
                    {
                        EventAction.Apply("Page_Before_Remove", page);
                        var oldTaxonomies = Repository.LoadCateogories(page.PageID);
                        var oldTags = Repository.LoadTags(page.PageID);
                        RelationShip.RemovePageRelationShips(page.PageID);
                        Comment.RemoveAll(page.PageID);
                        //Attachment.RemoveAll(page.PageID);
                        Repository.Remove(page);
                        EventAction.Apply("Page_Removed", page);
                        ts.Complete();
                        page.Deleted = true;
                        result = true;
                    }
                }
            }
            return result;
        }

        public int Count()
        {
            return Repository.Count(0);
        }

        public int DraftCount()
        {
            return Repository.Count(1);
        }

        public int TrashCount()
        {
            return Repository.Count(2);
        }

        public int PostedCount()
        {
            return Repository.Count(3);
        }

        public Page Get(int id, bool loadMeta = false)
        {
            var page = Repository.GetByID(id);
            if (loadMeta && page != null) LoadPageMeata(page, Repository);
            return page;
        }

        private Page GetCategoryIndex(string item)
        {
            //Here we render the pagecontent for displaying category's index
            //by Razor.Parse
            var page = LoadPage("category_index");
            return page;
        }

        /// <summary>
        /// Return the page for the request
        /// </summary>
        /// <param name="requestUrl">Incoming request</param>
        /// <returns>A Page<see cref="Page"/> instance</returns>
        public Page GetPage(string requestUrl)
        {
            return RouteHanlder(requestUrl);
        }

        //public string LoadTemplate(string file)
        //{
        //    var theme = SiteConfig.SiteTheme;
        //    var themeBase = System.Web.HttpContext.Current.Server.MapPath("~/Content/Themes/" + theme);
        //    var template = System.IO.File.ReadAllText(themeBase + "/" + file + ".cshtml");

        //    return null;
        //}

        public Page LoadPage(string pageName, bool loadMeta = false)
        {
            var page = Repository.GetByCondition(pageName);
            if (loadMeta && page != null) LoadPageMeata(page, Repository);
            return page;
        }

        public Page LoadPage(string category, string pageName, bool loadMeta = false)
        {
            var page = Repository.GetByCondition(pageName, author: null, category: category);
            if (loadMeta && page != null) LoadPageMeata(page, Repository);
            return page;
        }

        private void LoadPageMeata(Page page, IPageRepository repository)
        {
            page.Tags = repository.LoadTags(page.PageID);
            page.Taxonomies = repository.LoadCateogories(page.PageID);
            page.Meta = repository.LoadMeta(page.PageID);
        }

        public Page Load404()
        {
            return Repository.GetByCondition("HTTP404");
        }

        /// <summary>
        /// Default handler for routing
        /// We assue the url already be validated.
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <returns>The requested page</returns>
        internal Page RouteHanlder(string requestUrl)
        {
            /*
            %title%
            %id%
            %category%
            %year%
            %month%
            %day%
            
            Accept
            ====================
            1. %id%
            2. %title%
            3. %category%/%id%
            4. %category%/%title%
            5. %category%/%year%/(%title%|%id%)
            6. %category%/%year%/%month%/(%title%|%id%)
            7. %category%/%year%/%month%/%day%/(%title%|%id%)
            */

            var routingFormat = WPN.DbCfg.GetVal("routing").Replace("%", string.Empty);
            var items = requestUrl.Split('/');
            var expected_tags = routingFormat.Split('/');
            //%id% | %title%
            if (items.Length == 1)
            {
                //See whether it is a category or not
                if (Taxonomy.UrlAbbrExist(items[0]))
                {
                    return GetCategoryIndex(items[0]);
                }
                if (expected_tags[0] == "id")
                {
                    return Get(Int32.Parse(items[0]));
                }
                if (expected_tags[0] == "title")
                {
                    return LoadPage(items[0]);
                }
            }
            if (EventFilter.Has("route_handler"))
            {
                var page = EventFilter.Apply<Page>("route_handler", requestUrl);
                if (page != null)
                    return page;
            }
            string title = null, category = null, author = null;
            int? year = null, month = null, day = null, id = null;
            for (int i = 0; i < expected_tags.Length; i++)
            {
                switch (expected_tags[i])
                {
                    case "category":
                        category = items[i];
                        break;
                    case "title":
                        title = items[i];
                        break;
                    case "id":
                        id = Int32.Parse(items[i]);
                        break;
                    case "year":
                        year = Int32.Parse(items[i]);
                        break;
                    case "month":
                        month = Int32.Parse(items[i]);
                        break;
                    case "day":
                        month = Int32.Parse(items[i]);
                        break;
                    case "author":
                        author = items[i];
                        break;
                    default:
                        break;
                }
            }
            return Repository.GetByCondition(title, author, category, year, month, day, id);
        }

        public IEnumerable<Page> Search(
            string pageTitleContains,
            string pageContentContains = null,
            DateTime? dateBefore = null,
            DateTime? dateAfter = null,
            string tagName = null,
             string taxonomyName = null,
             int page = 0,
             bool loadMeta = false
            )
        {
            return Repository.Search(pageTitleContains, pageContentContains, dateBefore, dateAfter, tagName, taxonomyName, page, loadMeta);
        }

        public IEnumerable<Page> LoadAll(int pageNum, int amount)
        {
            return Repository.LoadAll(pageNum, amount);
        }

        public bool LoadCategories(Page page)
        {
            if (page.PageID > 0)
            {
                if (Repository.CheckExistByID(page.PageID))
                {
                    page.Taxonomies = Repository.LoadCateogories(page.PageID);
                    return true;
                }
            }
            return false;
        }

        public bool LoadMeta(Page page)
        {
            if (page.PageID > 0 && !page.Deleted)
            {
                if (Repository.CheckExistByID(page.PageID))
                {
                    page.Meta = Repository.LoadMeta(page.PageID);
                    return true;
                }
            }
            return false;
        }

        public bool LoadTags(Page page)
        {
            if (page.PageID > 0 && !page.Deleted)
            {
                if (Repository.CheckExistByID(page.PageID))
                {
                    page.Tags = Repository.LoadTags(page.PageID);
                    return true;
                }
            }
            return false;
        }

        public bool Save(Page page)
        {
            if (page.PageID > 0 && !page.Deleted)
            {
                if (Repository.CheckExistByID(page.PageID))
                {
                    //check if the page has been updated before the update
                    EventAction.Apply("Page_Before_Update", page);
                    if (Repository.Update(page))
                    {
                        //Update tags & taxonomies
                        RelationShip.RemovePageRelationShips(page.PageID);
                        RelationShip.AddPageTag(page);
                        RelationShip.AddTaxonomyPage(page);
                    }
                    EventAction.Apply("Page_After_Update", page);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Repository.Add(page);
                RelationShip.AddTaxonomyPage(page);
                RelationShip.AddPageTag(page);
                return true;
            }
        }
    }
}
