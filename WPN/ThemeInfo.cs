﻿using System.Collections.Generic;

namespace WPN
{
    public class ThemeInfo
    {
        public string ThemeName { get; set; }
        public int MajorVersion { get; set; }
        public int MinorVersion { get; set; }
        public string Author { get; set; }
        public string AuthorURI { get; set; }
        public string ThemeURI { get; set; }
        public string Desc { get; set; }
        public string ThemeFolder { get; set; }
        public string License { get; set; }
        public string LicenseURI { get; set; }
        public string[] Tags { get; set; }

        public Dictionary<string, dynamic> Libs { get; set; }
    }
}
