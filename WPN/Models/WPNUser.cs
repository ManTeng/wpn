﻿namespace WPN.Models
{
    public class WPNUser
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public string UserEmail { get; set; }
        public string UserNickName { get; set; }
        public string UserToken { get; set; }
        /// <summary>
        /// Admin = 1
        /// User = 2
        /// Reader = 3
        /// </summary>
        public int UserType { get; set; }



        internal bool CheckProperty()
        {
            return ( (UserName != null && UserName.Trim() != string.Empty)
                && (UserPwd != null && UserPwd.Trim() != string.Empty)
                && (UserEmail != null && UserEmail.Trim() != string.Empty)
                && (UserNickName != null && UserNickName.Trim() != string.Empty)
                );
        }

    }
}
