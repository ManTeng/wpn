﻿using System.Collections.Generic;
using WPN.DAL;

namespace WPN.Models
{
    public class Term
    {
        public const string SysPage = "Sys";
        public const string CustomPage = "Custom";
        public const string Trash = "Trash";
        public const string Tag = "Tag";
        public const string Taxonomy = "Taxonomy";
        public const string Menu = "Menu";


        public int TermID { get; set; }
        public string TermName { get; set; }
        public string UrlAbbr { get; set; }
        public string Desc { get; set; }

        internal static void Add(Term[] term)
        {
            ResolveService.Create<ITermRepository>().Add(term);
        }

        public void Add()
        {
            ResolveService.Create<ITermRepository>().Add(this);
        }

        public static IEnumerable<Taxonomy> GetTaxonomies(string termName, string taxonomyName)
        {
            //Get TermID by termName
            //Check if a taxonomy named taxonomyName exists with the TermID
            //-Y
            //--Get the first level children by the taxonomyid
            //---If 2nd level children exists, then the taxonomyids will be collected level by level, stored in an array
            //---Query the taxonomy table with in (int[])
            //-N
            //return null
            ITermRepository termRepository = ResolveService.Create<ITermRepository>();
            ITaxonomyRepository taxonomyRepository = ResolveService.Create<ITaxonomyRepository>();
            int termId = termRepository.GetID(termName);
            if (termId<=0)
            {
                return null;
            }
            int taxonomyId = taxonomyRepository.GetID(termId, taxonomyName);
            if (taxonomyId<=0)
            {
                return null;
            }
            return taxonomyRepository.GetChildren(taxonomyId);
        }
    }
}
