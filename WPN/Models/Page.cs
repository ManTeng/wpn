﻿using System;
using System.Collections.Generic;

namespace WPN.Models
{
    public class Page
    {

        public Page()
        {
            PostDate = DateTime.UtcNow;
        }
        public int TermID { get; set; }

        public int PageID { get; set; }
        public string PageName { get; set; }
        public string PageContent { get; set; }
        public DateTime PostDate { get; set; }
        public PageStatus PageStatus { get; set; }
        public string UrlAbbr { get; set; }
        public Byte[] PageTimeStamp { get; set; }
        public bool Deleted { get; set; }

        public IEnumerable<Tag> Tags { get; set; }
        public IEnumerable<Taxonomy> Taxonomies { get; set; }
        public IEnumerable<PostMeta> Meta { get; set; }

    }

    public enum PageStatus
    {
        Posted,
        Draft,
        Trash
    }
}