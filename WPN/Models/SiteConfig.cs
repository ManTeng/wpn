﻿namespace WPN.Models
{

    public class SiteConfig
    {
        public SiteConfig()
        {
            SystemConfig = false;
            Editable = true;

        }
        public int ID { get; set; }
        public string Item { get; set; }
        public string Value { get; set; }
        public string ItemLabel { get; set; }
        public string Description { get; set; }
        public bool SystemConfig { get; set; }
        public bool Editable { get; set; }
        public string SettingGroup { get; set; }
        
    }
}