﻿using WPN.DAL;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;

namespace WPN.Models
{
    public class Taxonomy
    {
        public Taxonomy()
        {
            
        }
        public int TaxonomyID { get; set; }
        public string TaxonomyName { get; set; }
        public string UrlAbbr { get; set; }
        public int ParentID { get; set; }
        public int TermID { get; set; }

        public static IEnumerable<Taxonomy> LoadLeagues()
        {
            return ResolveService.Create<ITaxonomyRepository>().LoadLeagues();
        }

        public static void Add(Taxonomy[] termsTaxonomy)
        {
            ResolveService.Create<ITaxonomyRepository>().Add(termsTaxonomy);
        }

        public static void Add(Taxonomy item)
        {
            ResolveService.Create<ITaxonomyRepository>().Add(item);
        }

        public static bool Exist(string taxonomyName)
        {
            return ResolveService.Create<ITaxonomyRepository>().Exist(taxonomyName);
        }

        public static bool UrlAbbrExist(string v)
        {
            return ResolveService.Create<ITaxonomyRepository>().UrlAbbrExist(v);
        }

        public static string GetTaxonomiesInJson(string term)
        {
            var list = ResolveService.Create<ITaxonomyRepository>().GetByTerm(term).ToList();
            return SimpleJSON.Parser.ToJson<List<Taxonomy>>(list);
        }
    }
}
