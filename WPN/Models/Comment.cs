﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WPN.DAL;

namespace WPN.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int PageID { get; set; }
        public DateTime CommentDate { get; set; }
        public int CommentUserID { get; set; }
        public int CommentUserName { get; set; }
        public string CommentEmail { get; set; }
        public string CommentWebSite { get; set; }
        public string CommentContent { get; set; }
        public CommentStatus Status { get; set; }

        public static void RemoveAll(int pageID)
        {
            ResolveService.Create<ICommentRepository>().RemoveAll(pageID);
        }

        public static void MarkSpam(int commentID)
        {
            if (commentID >0)
            {
                ResolveService.Create<ICommentRepository>().MarkSpam(commentID);
            }
        }

        public static void MarkNormal(int commentID)
        {
            if (commentID <= 0)
            {
                return;
            }
            ResolveService.Create<ICommentRepository>().MarkNormal(commentID);
        }

        public void Add()
        {
            if (PageID > 0 && CommentUserID > 0 && CommentEmail!=null && CommentContent!=null)
            {
                ResolveService.Create<ICommentRepository>().AddComment(this);
            }
        }

        public void Delete()
        {
            if (this.CommentID > 0)
            {
                ResolveService.Create<ICommentRepository>().Remove(CommentID);
            }
        }
    }

    public enum CommentStatus
    {
        Spam,
        Normal
    }
}
