﻿using WPN.DAL;

namespace WPN.Models
{
    public class RelationShip
    {
        public int TermID { get; set; }
        public int ValueID { get; set; }
        public int RefID { get; set; }
        public int Order { get; set; }

        public static void Add(RelationShip[] relationShip)
        {
            ResolveService.Create<IRelationShipRepository>().Add(relationShip);
        }

        public  static void RemovePageRelationShips(int pageID)
        {
            if (pageID>0)
            {
                ResolveService.Create<IRelationShipRepository>().RemoveTermRelationShip(Term.CustomPage, false, pageID);
                ResolveService.Create<IRelationShipRepository>().RemoveTermRelationShip(Term.Tag, true,  pageID);
            }
        }

        internal static void AddPageTag(Page page)
        {
            var repository = ResolveService.Create<IRelationShipRepository>();
            var termRespository = ResolveService.Create<ITermRepository>();
            var tagTermID = termRespository.GetID(Term.Tag);
            foreach (var tag in page.Tags)
            {
                repository.Add(new RelationShip { TermID = tagTermID, RefID = tag.TagID, ValueID = page.PageID });
            }
        }

        internal static void AddTaxonomyPage(Page page)
        {
            var repository = ResolveService.Create<IRelationShipRepository>();
            var termRespository = ResolveService.Create<ITermRepository>();
            var termid = termRespository.GetID(Term.Taxonomy);
            foreach (var category in page.Taxonomies)
            {
                repository.Add(new RelationShip { TermID = termid, RefID = category.TaxonomyID, ValueID=page.PageID });
            }
        }
    }
}
