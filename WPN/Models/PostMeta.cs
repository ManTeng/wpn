﻿using System;

namespace WPN.Models
{
    public class PostMeta
    {
        public int PostMetaID { get; set; }
        public int PostID { get; set; }
        public string MetaKey { get; set; }
        public string MetaValue { get; set; }
    }
}
