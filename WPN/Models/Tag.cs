﻿namespace WPN.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        public string TagName { get; set; }
        public int Count { get; set; }
    }
}