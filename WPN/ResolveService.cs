﻿using Bee.Core;
using System;

namespace WPN
{
    public static class ResolveService
    {
        public static T Create<T>()
        {
            return (T)EventFilter.Apply<object>("IOC_GetObject", default(T), typeof(T),null);
        }
    }

    public interface IResolveService
    {
        object Resolve(object computedResult, Type type, object parm = null);
    }
}
